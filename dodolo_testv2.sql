-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07 Jan 2019 pada 14.05
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dodolo_testv2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `a__admin`
--

CREATE TABLE `a__admin` (
  `id_account` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `active_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `a__admin`
--

INSERT INTO `a__admin` (`id_account`, `email`, `name`, `username`, `password`, `pin`, `token`, `is_active`, `active_at`, `created_at`, `update_at`) VALUES
(1, 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', NULL, 1, NULL, '0000-00-00 00:00:00', NULL),
(2, 'admin2@gmail.com', 'admin2', 'admin2', '827ccb0eea8a706c4c34a16891f84e7b', '', NULL, 1, NULL, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `a__payment_account`
--

CREATE TABLE `a__payment_account` (
  `id_payment_account` int(11) NOT NULL,
  `account_bank` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_branch` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `a__payment_account`
--

INSERT INTO `a__payment_account` (`id_payment_account`, `account_bank`, `account_number`, `account_name`, `account_branch`, `created_at`, `update_at`) VALUES
(1, 'Mandiri', '0700001855555', 'dodoloid', 'Malang', NULL, NULL),
(2, 'BCA', '6860148755', 'dodoloid', 'Malang', NULL, NULL),
(3, 'BNI', '0095555554', 'dodoloid', 'Malang', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `a__product`
--

CREATE TABLE `a__product` (
  `id` int(11) NOT NULL,
  `id_type` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `price_hpp` bigint(20) DEFAULT NULL,
  `markup` double(5,0) DEFAULT NULL,
  `markup_rp` bigint(20) DEFAULT NULL,
  `price_sell` bigint(20) DEFAULT NULL,
  `min_order` int(11) DEFAULT NULL,
  `max_order` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `a__product`
--

INSERT INTO `a__product` (`id`, `id_type`, `name`, `description`, `image`, `price_hpp`, `markup`, `markup_rp`, `price_sell`, `min_order`, `max_order`, `stock`, `is_active`, `created_at`, `update_at`) VALUES
(1, 2, 'product tes', 'ini product tes', 'tes_product.jpg', 13000, 10, 1300, 14300, 5, 15, 150, 0, '2019-01-02 22:11:40', '2019-01-07 09:07:42'),
(2, 1, 'tes product', 'ini tes product', 'tes_product.jpg', 15000, 10, 1500, 16500, 1, 10, 99, 1, '2019-01-03 01:08:34', '2019-01-07 09:05:48'),
(3, 3, 'ini product', 'tes kesehatan', 'ini_product.jpg', 15000, 10, 1500, 16500, 1, 10, 100, 0, '2019-01-03 16:36:28', '2019-01-07 09:07:41'),
(4, 1, 'tes product edit', 'ini tes product baru edit', '2095756274.jpg', 22000, 30, 6600, 28600, 5, 15, 144, 1, '2019-01-07 04:28:41', '2019-01-07 08:07:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `a__product_type`
--

CREATE TABLE `a__product_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `a__product_type`
--

INSERT INTO `a__product_type` (`id`, `name`, `order_number`, `created_at`, `update_at`) VALUES
(1, 'Makanan', NULL, NULL, NULL),
(2, 'Minuman', NULL, NULL, NULL),
(3, 'Kesehatan', NULL, NULL, NULL),
(4, 'Kecantikan', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m__member`
--

CREATE TABLE `m__member` (
  `id_member` int(11) NOT NULL,
  `balance` bigint(20) DEFAULT NULL,
  `profile_image` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `is_verifyemail` tinyint(1) NOT NULL,
  `verifyemail_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `active_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `m__member`
--

INSERT INTO `m__member` (`id_member`, `balance`, `profile_image`, `email`, `username`, `password`, `pin`, `token`, `is_verifyemail`, `verifyemail_at`, `is_active`, `active_at`, `created_at`, `update_at`) VALUES
(2, NULL, '', 'yusuf@gmail.com', 'yusuf', '827ccb0eea8a706c4c34a16891f84e7b', '', NULL, 0, NULL, 0, NULL, '2018-12-31 01:58:07', '2019-01-06 07:52:55'),
(3, 13000, 'fadli.jpg', 'fadli@gmail.com', 'fadli', '827ccb0eea8a706c4c34a16891f84e7b', '', NULL, 0, NULL, 0, NULL, '2019-01-01 17:25:09', '2019-01-06 08:28:22'),
(4, NULL, '', 'test@gmail.com', 'test', '827ccb0eea8a706c4c34a16891f84e7b', '', NULL, 0, NULL, 0, NULL, '2019-01-07 03:02:19', '2019-01-07 03:07:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m__orders`
--

CREATE TABLE `m__orders` (
  `id` int(11) NOT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `no_transaction` varchar(255) DEFAULT NULL,
  `qty_order` int(11) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `total` double(20,0) DEFAULT NULL,
  `is_processing` tinyint(1) DEFAULT NULL,
  `processing_at` datetime DEFAULT NULL,
  `is_completed` tinyint(1) DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `is_cancel` tinyint(1) DEFAULT NULL,
  `cancel_at` datetime DEFAULT NULL,
  `status` enum('PENDING','PROCESSING','COMPLETED','CANCELLED') NOT NULL,
  `action_by` int(11) DEFAULT NULL,
  `action_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `m__orders`
--

INSERT INTO `m__orders` (`id`, `id_product`, `id_member`, `date`, `no_transaction`, `qty_order`, `price`, `total`, `is_processing`, `processing_at`, `is_completed`, `completed_at`, `is_cancel`, `cancel_at`, `status`, `action_by`, `action_at`, `created_at`, `update_at`) VALUES
(1, 4, 3, '2019-01-07 13:27:17', '2147484848', 6, 22000, 132000, 0, '2019-01-07 19:52:41', 1, '2019-01-07 19:59:28', NULL, NULL, 'COMPLETED', 2, '2019-01-07 19:59:28', '2019-01-07 13:27:17', '2019-01-07 19:59:28'),
(2, 2, 3, '2019-01-07 18:11:36', '1147266133', 1, 15000, 15000, NULL, NULL, NULL, NULL, NULL, NULL, 'PENDING', NULL, NULL, '2019-01-07 18:11:36', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m__payment`
--

CREATE TABLE `m__payment` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `no_transaction` varchar(50) NOT NULL,
  `id_member` int(11) NOT NULL,
  `m_id_paymentaccount` int(11) NOT NULL,
  `a_id_paymentaccount` int(11) NOT NULL,
  `nominal_transfer` bigint(20) NOT NULL,
  `action_by` int(11) DEFAULT NULL,
  `action_at` datetime DEFAULT NULL,
  `file_proof` varchar(100) DEFAULT NULL,
  `status` enum('PENDING','PAID') DEFAULT 'PENDING',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `m__payment`
--

INSERT INTO `m__payment` (`id`, `date`, `no_transaction`, `id_member`, `m_id_paymentaccount`, `a_id_paymentaccount`, `nominal_transfer`, `action_by`, `action_at`, `file_proof`, `status`, `created_at`, `updated_at`) VALUES
(1, '2019-01-03 03:19:42', '2147484848', 3, 3, 1, 50000, 2, '2019-01-06 18:32:02', '2147484848.jpg', 'PAID', '2019-01-03 03:19:42', '2019-01-06 18:32:02'),
(2, '2019-01-06 18:36:14', '1049408028', 3, 1, 1, 10000, 2, '2019-01-06 18:36:42', '1049408028.jpg', 'PAID', '2019-01-06 18:36:14', '2019-01-06 18:36:42'),
(3, '2019-01-07 13:21:52', '1290280191', 3, 3, 3, 100000, 2, '2019-01-07 13:22:52', '1290280191.jpg', 'PAID', '2019-01-07 13:21:52', '2019-01-07 13:22:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m__payment_account`
--

CREATE TABLE `m__payment_account` (
  `id_payment_account` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `account_bank` varchar(100) DEFAULT NULL,
  `account_number` varchar(100) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_branch` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `m__payment_account`
--

INSERT INTO `m__payment_account` (`id_payment_account`, `id_member`, `account_bank`, `account_number`, `account_name`, `account_branch`, `created_at`, `update_at`) VALUES
(1, 3, 'Mandiri', '0700000899992', 'Akhmad', 'Malang', NULL, NULL),
(2, 2, 'BCA', '7310252527', 'Yusuf', 'Malang', NULL, NULL),
(3, 3, 'BNI', '0238272088', 'Akhmad', 'Malang', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a__admin`
--
ALTER TABLE `a__admin`
  ADD PRIMARY KEY (`id_account`) USING BTREE;

--
-- Indexes for table `a__payment_account`
--
ALTER TABLE `a__payment_account`
  ADD PRIMARY KEY (`id_payment_account`) USING BTREE;

--
-- Indexes for table `a__product`
--
ALTER TABLE `a__product`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `a__product_type`
--
ALTER TABLE `a__product_type`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m__member`
--
ALTER TABLE `m__member`
  ADD PRIMARY KEY (`id_member`) USING BTREE;

--
-- Indexes for table `m__orders`
--
ALTER TABLE `m__orders`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m__payment`
--
ALTER TABLE `m__payment`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `m__payment_account`
--
ALTER TABLE `m__payment_account`
  ADD PRIMARY KEY (`id_payment_account`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `a__admin`
--
ALTER TABLE `a__admin`
  MODIFY `id_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `a__payment_account`
--
ALTER TABLE `a__payment_account`
  MODIFY `id_payment_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `a__product`
--
ALTER TABLE `a__product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `a__product_type`
--
ALTER TABLE `a__product_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m__member`
--
ALTER TABLE `m__member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m__orders`
--
ALTER TABLE `m__orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m__payment`
--
ALTER TABLE `m__payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m__payment_account`
--
ALTER TABLE `m__payment_account`
  MODIFY `id_payment_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
