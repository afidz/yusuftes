<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller  {
	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') != TRUE) {
			redirect('login');
		}
		$this->load->model('m_user');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function index() {
		$this->load->view('user.bak.php');
	}
	
	public function producer() {
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-producer';
        $data['title']='Data Produsen';
		$data['content'] = $this->load->view('/producer',$data,TRUE);		
        $this->load->view('template',$data);
	}

	public function producer_detail() {
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-producer';
        $data['title']='Data Produsen';
        $data['content'] = $this->load->view('/detail-producer.php',$data,TRUE);
        $this->load->view('template',$data);
	}

	public function reseller() {
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-reseller';
        $data['title']='Data Reseller';
        $data['content'] = $this->load->view('/reseller',$data,TRUE);
        $this->load->view('template',$data);
	}

	public function reseller_detail() {
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-reseller';
        $data['title']='Data Reseller';
        $data['content'] = $this->load->view('/detail-reseller.php',$data,TRUE);
        $this->load->view('template',$data);
	}
	
	public function ajax_action_update_status_producer() {
		if(post('csrf') == $this->security->get_csrf_hash()) {		
			$id = post('id');
			$value = post('value');
			
			if($value == 0) {
				$new_value = 1;
			} else {
				$new_value = 0;
			}
			
			$data = array(
				'is_active' => $new_value,
				'active_at' => date('Y-m-d H:i:s')
			);
			$query = $this->m_user->edit_producer_account($id, $data);
			
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Status gagal diubah!'),
				'form_error' => ''
			);
			if(!$query) die(json_encode($json_data));
			
			$json_data = array(
				'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Status berhasil diubah!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_action_update_status_reseller() {
		if(post('csrf') == $this->security->get_csrf_hash()) {		
			$id = post('id');
			$value = post('value');
			
			if($value == 0) {
				$new_value = 1;
			} else {
				$new_value = 0;
			}
			
			$data = array(
				'is_active' => $new_value,
				'active_at' => date('Y-m-d H:i:s')
			);
			$query = $this->m_user->edit_reseller_account($id, $data);
			
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Status gagal diubah!'),
				'form_error' => ''
			);
			if(!$query) die(json_encode($json_data));
			
			$json_data = array(
				'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Status berhasil diubah!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_get_producer() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$data = $this->db->select('*')
					->from('pro__user u')
					->join('pro__account a', 'a.id_user = u.id_user')
					->join('pro__company c', 'c.id_user = u.id_user')
					->order_by('u.created_at', 'DESC')
					->get()
					->result();
			$json_data = array(
				'result' => TRUE,
				'data' => $data
			);
			
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_producer_by_id() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			
			$data = $this->db->select(
						'*,
						u.phone AS phone,
						u.phone2 AS phone2,
						u.address AS address,
						p.name AS province,
						r.name AS regency,
						d.name AS district,
						v.name AS village,
						r1.name AS birth_regency,
						j.name AS job,
						e.name AS education,
						c.name AS comp_name,
						c.phone AS comp_phone,
						c.phone2 AS comp_phone2,
						c.address AS comp_address,
						pc.name AS comp_province,
						rc.name AS comp_regency,
						dc.name AS comp_district,
						vc.name AS comp_village,'
					)
					->from('pro__user u')
					->join('sys__provinces p', 'p.id = u.id_provinces')
					->join('sys__regencies r', 'r.id = u.id_regency')
					->join('sys__districts d', 'd.id = u.id_district')
					->join('sys__villages v', 'v.id = u.id_village')
					->join('sys__regencies r1', 'r1.id = u.birth_regency')
					->join('sys__jobs j', 'j.id = u.id_job')
					->join('sys__educations e', 'e.id = u.id_education')
					->join('pro__company c', 'c.id_user = u.id_user')
					->join('sys__provinces pc', 'pc.id = c.id_provinces')
					->join('sys__regencies rc', 'rc.id = c.id_regency')
					->join('sys__districts dc', 'dc.id = c.id_district')
					->join('sys__villages vc', 'vc.id = c.id_village')
					->where('u.id_user', $id)
					->get()
					->row();
					
			$json_data = array(
				'result' => TRUE,
				'data' => $data
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_payment_by_producer() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			$data = $this->db->select('*, p.id AS payment_id')
					->from('pro__payment_account p')
					->join('sys__bank b', 'b.id = p.id_bank')
					->where('p.id_producer', $id)
					->get()
					->result();
					
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_photo_by_producer() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			
			$data = $this->db->select('*')
					->from('pro__images i')
					->where('i.id_user', $id)
					->get()
					->row();
					
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_reseller() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$data = $this->db->select('*')
					->from('res__user u')
					->join('res__account a', 'a.id_user = u.id_user')
					->order_by('u.created_at', 'DESC')
					->get()
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_reseller_by_id() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			
			$data = $this->db->select(
						'*,
						p.name AS province,
						r.name AS regency,
						d.name AS district,
						v.name AS village,
						r1.name AS birth_regency,
						j.name AS job,
						e.name AS education,'
					)
					->from('res__user u')
					->join('sys__provinces p', 'p.id = u.id_provinces')
					->join('sys__regencies r', 'r.id = u.id_regency')
					->join('sys__districts d', 'd.id = u.id_district')
					->join('sys__villages v', 'v.id = u.id_village')
					->join('sys__regencies r1', 'r1.id = u.birth_regency')
					->join('sys__jobs j', 'j.id = u.id_job')
					->join('sys__educations e', 'e.id = u.id_education')
					->where('u.id_user', $id)
					->get()
					->row();
					
			$json_data = array(
				'result' => TRUE,
				'data' => $data
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_payment_by_reseller() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			$data = $this->db->select('*, p.id AS payment_id')
					->from('res__payment_account p')
					->join('sys__bank b', 'b.id = p.id_bank')
					->where('p.id_reseller', $id)
					->get()
					->result();
					
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_photo_by_reseller() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			
			$data = $this->db->select('*')
					->from('res__images i')
					->where('i.id_user', $id)
					->get()
					->row();
					
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
}