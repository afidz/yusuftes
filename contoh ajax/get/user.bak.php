<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>User</h1>

	<div id="body">
		<h2>Producer</h2>
		<table border="1" width="100%" id="producer-table">
			<thead>
				<th>No.</th>
				<th>Name</th>
				<th>Email</th>
				<th>Company</th>
				<th>Action</th>
			</thead>
			<tbody>
			</tbody>
		</table>
		<h2>Reseller</h2>
		<table border="1" width="100%" id="reseller-table">
			<thead>
				<th>No.</th>
				<th>Name</th>
				<th>Email</th>
				<th>Action</th>
			</thead>
			<tbody>
			</tbody>
		</table>
		<input type="hidden" id="csrf" name="csrf" value="<?=$this->security->get_csrf_hash();?>">
	</div>
	
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<script src="<?php echo base_url(); ?>assets/front/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = $('#csrf').val();
	
	$.ajax({
		type: 'POST',
		url: base_url + 'admin.php/user/ajax_get_producer',
		data: {csrf: csrf},
		async: false,
		dataType: 'json',
		success: function(data) {
			var html = '';
			var i, label;
			var no = 1;
			for(i = 0; i < data.length; i++) {
				if(data[i].is_active == 0) {
					label = 'Activated'
				} else {
					label = 'Not activated'
				};
				
				html += '<tr>' +
							'<td>' + no + '</td>' +
							'<td>' + data[i].first_name + '&nbsp;' + data[i].last_name + '</td>' +
							'<td>' + data[i].email + '</td>' +
							'<td>' + data[i].name + '</td>' +
							'<td>' +
								'<button type="button" data-id="' + data[i].id_user + '" data-value="' + data[i].is_active + '" class="change-status-producer">' +
									label +
								'</button>' +
								'&nbsp|&nbsp' +
								'<a href="' + base_url + 'admin.php/user/producer/' + data[i].id_user + '">Detail</a>' +
							'</td>' +
						'</tr>';
				no++;
			}
			$('#producer-table > tbody').html(html);
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('.change-status-producer').click(function(){
		var id = $(this).data('id');
		var value = $(this).data('value');
		$.ajax({
			type: 'POST',
			url: base_url + 'admin.php/user/ajax_action_update_status_producer',
			data: {id: id, value: value, csrf: csrf},
			dataType: 'json',
			success: function(response) {
				alert(response.message.body);
				location.reload();
			},
			error: function() {
				alert('Interrupted');
			}
		})
	});
	
	$.ajax({
		type: 'POST',
		url: base_url + 'admin.php/user/ajax_get_reseller',
		data: {csrf: csrf},
		async: false,
		dataType: 'json',
		success: function(data) {
			var html = '';
			var i, label;
			var no = 1;
			for(i = 0; i < data.length; i++) {
				if(data[i].is_active == 0) {
					label = 'Activated'
				} else {
					label = 'Not activated'
				};
				
				html += '<tr>' +
							'<td>' + no + '</td>' +
							'<td>' + data[i].first_name + '&nbsp;' + data[i].last_name + '</td>' +
							'<td>' + data[i].email + '</td>' +
							'<td>' +
								'<button type="button" data-id="' + data[i].id_user + '" data-value="' + data[i].is_active + '" class="change-status-reseller">' +
									label +
								'</button>' +
								'&nbsp|&nbsp' +
								'<a href="' + base_url + 'admin.php/user/reseller/' + data[i].id_user + '">Detail</a>' +
							'</td>' +
						'</tr>';
				no++;
			}
			$('#reseller-table > tbody').html(html);
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('.change-status-reseller').click(function(){
		var id = $(this).data('id');
		var value = $(this).data('value');
		$.ajax({
			type: 'POST',
			url: base_url + 'admin.php/user/ajax_action_update_status_reseller',
			data: {id: id, value: value, csrf: csrf},
			dataType: 'json',
			success: function(response) {
				alert(response.message.body);
				location.reload();
			},
			error: function() {
				alert('Interrupted');
			}
		})
	});
});
</script>
</body>
</html>