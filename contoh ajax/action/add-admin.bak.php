<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Admin</h1>

	<div id="body">
		<?php $attributes = array('id' => 'myform'); ?>
		<?php echo form_open('', $attributes); ?>
			<label for="adm_first_name">First Name</label>
			<input type="text" id="adm_first_name" name="adm_first_name">
			<label for="adm_last_name">Last Name</label>
			<input type="text" id="adm_last_name" name="adm_last_name">
			<label for="adm_birth_place">Birth Place</label>
			<select id="adm_birth_place" name="adm_birth_place">
				<option value="">- Select Birth Place -</option>
				<?php foreach($regency as $data) { ?>
					<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
				<?php } ?>
			</select>
			<label for="adm_birth_date">Birth Date</label>
			<input type="text" id="adm_birth_date" name="adm_birth_date">
			<label for="adm_male">Male</label>
			<input type="radio" id="adm_male" name="adm_gender" value="L">
			<label for="adm_female">Female</label>
			<input type="radio" id="adm_female" name="adm_gender" value="P">
			<br>
			<label for="adm_email">Email</label>
			<input type="email" id="adm_email" name="adm_email">
			<label for="adm_phone">Phone 1</label>
			<input type="text" id="adm_phone" name="adm_phone">
			<label for="adm_phone2">Phone 2</label>
			<input type="text" id="adm_phone2" name="adm_phone2">
			<label for="adm_address">Address</label>
			<input type="text" id="adm_address" name="adm_address">
			<br>
			<label for="adm_province">Province</label>
			<select id="adm_province" name="adm_province">
				<option value="">- Select Province -</option>
				<?php foreach($province as $data) { ?>
					<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
				<?php } ?>
			</select>
			<label for="adm_regency">Regency</label>
			<select id="adm_regency" name="adm_regency">
				<option value="">- Select Regency -</option>
			</select>
			<label for="adm_district">District</label>
			<select id="adm_district" name="adm_district">
				<option value="">- Select District -</option>
			</select>
			<label for="adm_village">Village</label>
			<select id="adm_village" name="adm_village">
				<option value="">- Select Village -</option>
			</select>
			<br>
			<label for="adm_job">Job</label>
			<select id="adm_job" name="adm_job">
				<option value="">- Select Job -</option>
				<?php foreach($job as $data) { ?>
					<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
				<?php } ?>
			</select>
			<label for="adm_education">Education</label>
			<select id="adm_education" name="adm_education">
				<option value="">- Select Education -</option>
				<?php foreach($education as $data) { ?>
					<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
				<?php } ?>
			</select>
			<label for="adm_fb">Facebook</label>
			<input type="text" id="adm_fb" name="adm_fb">
			<label for="adm_ig">Instagram</label>
			<input type="text" id="adm_ig" name="adm_ig">
			<label for="adm_line">Line</label>
			<input type="text" id="adm_line" name="adm_line">
			<br><br>
			<label for="adm_username">Username</label>
			<input type="text" id="adm_username" name="adm_username">
			<label for="adm_password">Password</label>
			<input type="password" id="adm_password" name="adm_password">
			<label for="re_adm_password">Re-type New Password</label>
			<input type="password" id="re_adm_password">
			<span id="verify_password"></span>
			<br><br>
			<label for="adm_level">User Level</label>
			<select id="adm_level" name="adm_level">
			</select>
			<br><br>
			<input type="hidden" id="do_id" name="do_id" value="<?php echo session('do_id'); ?>">
			<input type="hidden" id="csrf" name="csrf" value="<?=$this->security->get_csrf_hash();?>">
			<button type="submit" id="submit">Submit</button>
		<?php echo form_close(); ?>
		<div id="session_message"></div>
	</div>
	
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>
<script src="<?php echo base_url(); ?>assets/front/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = $('#csrf').val();
	
	if($('#do_id').val() != '') {
		$('#session_message').html('Your session has been actived');
	} else {
		$('#session_message').html('Your session was expired');
	}
	
	$.ajax({
		type: 'POST',
		url: base_url + 'admin.php/user/admin/ajax_get_level',
		data: {csrf: csrf},
		async: false,
		dataType: 'json',
		success: function(data) {
			var html = '<option value="">- Select Level -</option>';
			var i;
			for(i = 0; i < data.length; i++) {
				html += '<option value="' + data[i].id_account_level + '">' + data[i].name + '</option>';
			}
			$('#adm_level').html(html);
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('#adm_province').change(function() {
		var id = $(this).val();
		$.ajax({
			type: 'POST',
			url: base_url + 'admin.php/user/admin/ajax_get_regency',
			data: {id: id, csrf: csrf},
			async: false,
			dataType: 'json',
			success: function(data) {
				var html = '<option value="">- Select Regency -</option>';
				var i;
				for(i = 0; i < data.length; i++) {
					html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
				}
				$('#adm_regency').html(html);
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});
	
	$('#adm_regency').change(function() {
		var id = $(this).val();
		$.ajax({
			type: 'POST',
			url: base_url + 'admin.php/user/admin/ajax_get_district',
			data: {id: id, csrf: csrf},
			async: false,
			dataType: 'json',
			success: function(data) {
				var html = '<option value="">- Select District -</option>';
				var i;
				for(i = 0; i < data.length; i++) {
					html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
				}
				$('#adm_district').html(html);
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});
	
	$('#adm_district').change(function() {
		var id = $(this).val();
		$.ajax({
			type: 'POST',
			url: base_url + 'admin.php/user/admin/ajax_get_village',
			data: {id: id, csrf: csrf},
			async: false,
			dataType: 'json',
			success: function(data) {
				var html = '<option value="">- Select Village -</option>';
				var i;
				for(i = 0; i < data.length; i++) {
					html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
				}
				$('#adm_village').html(html);
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});
	
	$('#re_adm_password').keyup(function() {
		var pwd = $('#adm_password').val();
		if(pwd != '') {
			if($(this).val() != pwd) {
				$('#verify_password').html('This value is not same with your password!');
				$('#submit').prop('disabled', true);
			} else {
				$('#verify_password').html('');
				$('#submit').prop('disabled', false);
			}
		}
	});
	
	$('#adm_password').keyup(function() {
		var re_pwd = $('#re_adm_password').val();
		if(re_pwd == '') {
			$('#submit').prop('disabled', true);
			$('#verify_password').html('This field is required!');
		}
	});
	
	$('#myform').submit(function(event) {
		if($('#do_id').val() != '') {	
			event.preventDefault();
			var datastring = $(this).serialize();
			$.ajax({
				type: 'POST',
				url: base_url + 'admin.php/user/admin/ajax_action_insert',
				dataType: 'json',
				data: datastring,
				success: function(response) {
					alert(response.message.body);
					if(response.redirect != '') {
						window.location.replace(base_url + response.redirect);
					}		
					if(!response.result){
						foreach(response.form_error)
					}			
				},
				error: function(response) {
					alert('Interrupted');
				}
			});
		} else {
			alert('Your session was expired. Please login first!')
		}
	});
});
</script>
</body>
</html>