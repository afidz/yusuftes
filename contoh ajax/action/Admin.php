<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller  {
	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') != TRUE) {
			redirect('login');
		}
		$this->load->model(array('m_admin', 'm_job', 'm_education', 'm_province', 'm_regency'));
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function index() {
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-admin';
        $data['title']='Data Admin';
        $data['content'] = $this->load->view('/admin.php',$data,TRUE);
        // $data['content'] = $this->load->view('/admin.bak.php',$data,TRUE);
        $this->load->view('template',$data);
	}
	
	public function add() {
		$data['province'] = $this->m_province->get_province();
		$data['regency'] = $this->m_regency->get_regency();
		$data['job'] = $this->m_job->get_job();
		$data['education'] = $this->m_education->get_education();
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-admin';
        $data['title']='Tambah Admin';
        $data['content'] = $this->load->view('/add-admin.php',$data,TRUE);
        // $data['content'] = $this->load->view('/add-admin.bak.php',$data,TRUE);
        $this->load->view('template',$data);
	}
	
	public function edit() {
		$data['province'] = $this->m_province->get_province();
		$data['regency'] = $this->m_regency->get_regency();
		$data['job'] = $this->m_job->get_job();
		$data['education'] = $this->m_education->get_education();
		$data['sidebar_active']='user';
		$data['sidebar_submenu']='user-admin';
        $data['title']='Edit Admin';
        $data['content'] = $this->load->view('/edit-admin.php',$data,TRUE);
        // $data['content'] = $this->load->view('/edit-admin.bak.php',$data,TRUE);
        $this->load->view('template',$data);
	}
	
	public function ajax_action_insert() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$first_name = post('adm_first_name');
			$last_name = post('adm_last_name');
			$birth_place = post('adm_birth_place');
			$birth_date = post('adm_birth_date');
			$gender = post('adm_gender');
			$email = post('adm_email');
			$phone = post('adm_phone');
			$phone2 = post('adm_phone2');
			$address = post('adm_address');
			$province = post('adm_province');
			$regency = post('adm_regency');
			$district = post('adm_district');
			$village = post('adm_village');
			$job = post('adm_job');
			$education = post('adm_education');
			$fb = post('adm_fb');
			$ig = post('adm_ig');
			$line = post('adm_line');
			
			$username = post('adm_username');
			$password = post('adm_password');
			
			//$level = post('adm_level');
			
			$date_time = date('Y-m-d H:i:s');
			
			$this->form_validation->set_rules('adm_first_name', 'First Name', 'required');
			$this->form_validation->set_rules('adm_last_name', 'Last Name', 'required');
			$this->form_validation->set_rules('adm_birth_place', 'Birth Place', 'required');
			$this->form_validation->set_rules('adm_birth_date', 'Birth Date', 'required');
			$this->form_validation->set_rules('adm_gender', 'Gender', 'required');
			$this->form_validation->set_rules('adm_email', 'Email', 'required');
			$this->form_validation->set_rules('adm_phone', 'Phone', 'required');
			$this->form_validation->set_rules('adm_address', 'Address', 'required');
			$this->form_validation->set_rules('adm_province', 'Province', 'required');
			$this->form_validation->set_rules('adm_regency', 'Regency', 'required');
			$this->form_validation->set_rules('adm_district', 'District', 'required');
			$this->form_validation->set_rules('adm_village', 'Village', 'required');
			$this->form_validation->set_rules('adm_job', 'Job', 'required');
			$this->form_validation->set_rules('adm_education', 'Education', 'required');
			
			$this->form_validation->set_rules('adm_username', 'Username', 'required');
			$this->form_validation->set_rules('adm_password', 'Password', 'required');
			
			//$this->form_validation->set_rules('adm_level', 'User Level', 'required');
			
			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {
				$check_email = $this->m_admin->check_email($email);
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Email anda telah terdaftar!'),
					'form_error' => '',
					'redirect' => ''
				);				
				if($check_email == TRUE) die(json_encode($json_data));
				
				$check_username = $this->m_admin->check_username($username);				
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Username ini telah digunakan!'),
					'form_error' => '',
					'redirect' => ''
				);				
				if($check_username == TRUE) die(json_encode($json_data));
				
				$user_data = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'birth_regency' => $birth_place,
					'birth_date' => $birth_date,
					'gender' => $gender,
					'email' => $email,
					'phone' => $phone,
					'phone2' => $phone2,
					'address' => $address,
					'id_provinces' => $province,
					'id_regency' => $regency,
					'id_district' => $district,
					'id_village' => $village,
					'id_job' => $job,
					'id_education' => $education,
					'social_fb' => $fb,
					'social_ig' => $ig,
					'social_line' => $line,
					'created_at' => $date_time,
				);
				
				$account_data = array(
					//'id_level ' => $level,
					'username ' => $username,
					'password' => md5($password),
					'created_at' => $date_time,
				);				
				$insert = $this->m_admin->add_data($user_data, $account_data);
				
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal disimpan!'),
					'form_error' => '',
					'redirect' => ''
				);				
				if($insert == FALSE) die(json_encode($json_data));
				
				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses disimpan!'),
					'form_error' => '',
					'redirect' => $this->config->item('index_page').'/user/admin'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_action_update() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('adm_id');
			$first_name = post('adm_first_name');
			$last_name = post('adm_last_name');
			$birth_place = post('adm_birth_place');
			$birth_date = post('adm_birth_date');
			$gender = post('adm_gender');
			//$email = post('adm_email');
			$phone = post('adm_phone');
			$phone2 = post('adm_phone2');
			$address = post('adm_address');
			$province = post('adm_province');
			$regency = post('adm_regency');
			$district = post('adm_district');
			$village = post('adm_village');
			$job = post('adm_job');
			$education = post('adm_education');
			$fb = post('adm_fb');
			$ig = post('adm_ig');
			$line = post('adm_line');
			
			//$username = post('adm_username');
			$password = post('adm_password');
			
			//$level = post('adm_level');
			
			$date_time = date('Y-m-d H:i:s');
			
			$this->form_validation->set_rules('adm_first_name', 'First Name', 'required');
			$this->form_validation->set_rules('adm_last_name', 'Last Name', 'required');
			$this->form_validation->set_rules('adm_birth_place', 'Birth Place', 'required');
			$this->form_validation->set_rules('adm_birth_date', 'Birth Date', 'required');
			$this->form_validation->set_rules('adm_gender', 'Gender', 'required');
			//$this->form_validation->set_rules('adm_email', 'Email', 'required');
			$this->form_validation->set_rules('adm_phone', 'Phone', 'required');
			$this->form_validation->set_rules('adm_address', 'Address', 'required');
			$this->form_validation->set_rules('adm_province', 'Province', 'required');
			$this->form_validation->set_rules('adm_regency', 'Regency', 'required');
			$this->form_validation->set_rules('adm_district', 'District', 'required');
			$this->form_validation->set_rules('adm_village', 'Village', 'required');
			$this->form_validation->set_rules('adm_job', 'Job', 'required');
			$this->form_validation->set_rules('adm_education', 'Education', 'required');
			
			//$this->form_validation->set_rules('adm_username', 'Username', 'required');
			//$this->form_validation->set_rules('adm_password', 'Password', 'required');
			
			//$this->form_validation->set_rules('adm_level', 'User Level', 'required');
			
			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {				
				$user_data = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'birth_regency' => $birth_place,
					'birth_date' => $birth_date,
					'gender' => $gender,
					//'email' => $email,
					'phone' => $phone,
					'phone2' => $phone2,
					'address' => $address,
					'id_provinces' => $province,
					'id_regency' => $regency,
					'id_district' => $district,
					'id_village' => $village,
					'id_job' => $job,
					'id_education' => $education,
					'social_fb' => $fb,
					'social_ig' => $ig,
					'social_line' => $line,
					'update_at' => $date_time,
				);
				
				if(!empty($password)) {
					$account_data = array(
						//'id_level ' => $level,
						//'username ' => $username,
						'password' => md5($password),
						'update_at' => $date_time,
					);
					
					$update = $this->m_admin->edit_data($id, $user_data, $account_data);
				} else {
					/*$account_data = array(
						'id_level ' => $level,
						'update_at' => $date_time,
					);*/
					//$update = $this->m_admin->edit_data($id, $user_data, $account_data);
					$update = $this->m_admin->edit_user($id, $user_data);
				}
				
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal diubah!'),
					'form_error' => '',
					'redirect' => ''
				);				
				if($update == FALSE) die(json_encode($json_data));
				
				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses diubah!'),
					'form_error' => '',
					'redirect' => $this->config->item('index_page').'/user/admin'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_action_delete() {
		if(post('csrf') == $this->security->get_csrf_hash()) {		
			$id = post('id');
			$query = $this->m_admin->remove_data($id);
			
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Data gagal dihapus!'),
				'form_error' => ''
			);
			if(!$query) die(json_encode($json_data));
			
			$json_data = array(
				'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Data sukses dihapus!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_get_admin() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$data = $this->db->select('*')
					->from('do__user u')
					->join('do__account a', 'a.id_user = u.id_user')
					->order_by('u.created_at', 'DESC')
					->get()
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_admin_by_id() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');			
			$data = $this->db->select('*')
					->from('do__user u')
					->join('do__account a', 'a.id_user = u.id_user')
					->where('u.id_user', $id)
					->get()
					->row();
					
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_regency(){
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			$data = $this->db->where('province_id', $id)
					->get('sys__regencies')
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_district() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			$data = $this->db->where('regency_id', $id)
					->get('sys__districts')
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_get_village() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			$data = $this->db->where('district_id', $id)
					->get('sys__villages')
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_get_level() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$data = $this->db->select('*')
					->from('do__account_level al')
					->get()
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
    }
	
	public function ajax_get_menu_by_user() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$user = session('do_id');
			$data = $this->db->select('*')
					->from('sys__app_menu am')
					->join('do__account_level_details ald', 'ald.id_menu = am.id_menu')
					->join('do__account a', 'a.id_level = ald.id_account_level')
					->where('a.id_user', $user)
					->where('am.id_app', 'dodolo')
					->where('am.is_parent', 1)
					->order_by('am.id_menu', 'ASC')
					->get()
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
	
	public function ajax_get_child_menu_by_parent() {
		if(post('csrf') == $this->security->get_csrf_hash()) {
			$id = post('id');
			$data = $this->db->select('*')
					->from('sys__app_menu am')
					->where('am.id_menu_parent', $id)
					->where('am.id_menu_parent != ', 2)
					->get()
					->result();
			print json_encode($data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => ''
			);
			print json_encode($json_data);
		}
	}
}