<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin | <?= $title; ?></title>
        
        <?= $this->load->view('template-head.php', TRUE); ?>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">

                <!-- Logo -->
                <a href="<?= base_url(); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Dodolo</b>Id</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="<?= base_url(); ?>">Home</a>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= base_url(); ?>assets/images/reseller/profile/default-160x160.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?= $this->session->userdata('username'); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= base_url(); ?>assets/images/reseller/profile/default-160x160.png" class="img-circle" alt="User Image">
                                        <p><?= $this->session->userdata('username'); ?></p>
                                    </li>
                                    <!-- end .user-header -->

                                    <!-- Menu Body -->
                                    
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left"></div>

                                        <div class="pull-right">
                                            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>

                                    </li>
                                    <!-- end .user-footer -->
                                </ul>
                                <!-- end .dropdown-menu -->
                            </li>
                            <!-- end .dropdown -->
                        </ul>
                        <!-- end .nav navbar-nav -->
                    </div>
                    <!-- end .navbar-custom-menu -->
                </nav>
                <!-- end .navbar -->
            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= base_url(); ?>assets/images/reseller/profile/default-160x160.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?= $this->session->userdata('username'); ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>

                        <li class="<?php if($sibebar_menu_active=="home"){echo "active";}?> treeview">
                            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/home">
                                <i class="fa fa-dashboard"></i> <span>Home</span>
                            </a>
                        </li>

                        <li class="<?php if($sibebar_menu_active=="member"){echo "active";}?> treeview">
                            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/member">
                                <i class="fa fa-dashboard"></i> <span>Member</span>
                            </a>
                        </li>

                        <li class="<?php if($sibebar_menu_active=="product"){echo "active";}?> treeview">
                            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/product">
                                <i class="fa fa-dashboard"></i> <span>Product</span>
                            </a>
                        </li>

                        <li class="<?php if($sibebar_menu_active=="payment"){echo "active";}?> treeview">
                            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/payment">
                                <i class="fa fa-dashboard"></i> <span>Payment</span>
                            </a>
                        </li>

                        <li class="<?php if($sibebar_menu_active=="order"){echo "active";}?> treeview">
                            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/order">
                                <i class="fa fa-dashboard"></i> <span>Order</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?= $content_header; ?>
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?= $content_header; ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">

                        <?= $content; ?>
                        

    </body>
</html>
