<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session_admin') != TRUE) {
			redirect('login');
        }
        $this->load->model(array('M_admin', 'M_member', 'M_orders', 'M_product'));
		date_default_timezone_set('Asia/Jakarta');
    }
    
	public function index()
	{
		$data['sibebar_menu_active'] = 'order';
        $data['title'] = 'Order';
        $data['content_header'] = 'Order';
        $data['content'] = $this->load->view('/order','', TRUE);
        
        $this->load->view('template', $data);
    }

    public function detail() {

		$data['no_transaction'] = segment(3);
		$data['sibebar_menu_active'] = 'order';
        $data['title'] = 'Order';
        $data['content_header'] = 'Order';
        $data['content'] = $this->load->view('/detail',$data, TRUE);
        
        $this->load->view('template', $data);
    }

    public function ajax_get_order_by_no_transaction()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$data = $this->M_orders->get_order_by_no_transaction(post('no_transaction'));

			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Data Tidak Ditemukan'),
				'form_error' => $data,
				'redirect' => $this->config->item('index_page').'/order'
			);				
			if($data != TRUE) die(json_encode($json_data));

			$detail = [];
            $member = $this->M_member->check_id_member($data['id_member']);
            $product = $this->M_product->check_id_product($data['id_product']);
			
            $detail['member'] = $member['username'];
            $detail['product'] = $product['name'];

			if($data['action_by'] != "") {
				$admin = $this->M_admin->check_id_account($data['action_by']);
				$detail['action_by'] = $admin['name'];
			} else {
				$detail['action_by'] = "Belum Ada";
			}

			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'data' => $data,
				'detail' => $detail
			);
			print json_encode($json_data);

		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => ''
			);
			print json_encode($json_data);
		}
    }

    public function ajax_get_order()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {
            
            $data = $this->M_orders->get_all_order();

            $product = $this->M_product->get_all_product();
					
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
                'data' => $data,
                'product' => $product
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
    }

    public function ajax_action_processing_order()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$date_time = date('Y-m-d H:i:s');
			$no_transaction = post('no_transaction');

			$order_data = array(
                'status' => 'PROCESSING',
                'is_processing' => '1',
                'processing_at' => $date_time,
				'action_by' => $this->session->userdata('id_admin'),
				'action_at' => $date_time,
				'update_at' => $date_time
			);
			
            $edit = $this->M_orders->edit_order($order_data, $no_transaction);

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Data order gagal DiProcessing!'),
                'form_error' => $edit,
                'redirect' => ''
            );				
			if($edit == FALSE) die(json_encode($json_data));

			$json_data = array(
				'csrf_token' => 'csrf token sama',
                'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Data order Berhasil DiProcessing!'),
				'form_error' => $edit,
                'redirect' => $this->config->item('index_page').'/order'
			);
            print json_encode($json_data);
            
		} else {

			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
            print json_encode($json_data);
            
		}
    }
    
    public function ajax_action_completed_order()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$date_time = date('Y-m-d H:i:s');
			$no_transaction = post('no_transaction');

			$order_data = array(
                'status' => 'COMPLETED',
                'is_processing' => '0',
                'is_completed' => '1',
                'completed_at' => $date_time,
				'action_by' => $this->session->userdata('id_admin'),
				'action_at' => $date_time,
				'update_at' => $date_time
			);
			
            $edit = $this->M_orders->edit_order($order_data, $no_transaction);

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Data order gagal DiCompleted!'),
                'form_error' => $edit,
                'redirect' => ''
            );				
			if($edit == FALSE) die(json_encode($json_data));

			$json_data = array(
				'csrf_token' => 'csrf token sama',
                'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Data order Berhasil DiCompleted!'),
				'form_error' => $edit,
                'redirect' => $this->config->item('index_page').'/order'
			);
            print json_encode($json_data);
            
		} else {

			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
            print json_encode($json_data);
            
		}
	}

}
