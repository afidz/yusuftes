<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Order</h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
        
            <table id="order_table" class="table table-bordered table-striped"></table>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/order/ajax_get_order',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {
                buildTable(response.data, response.product);
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});

    function buildTable(data, product) {
    
        var html = '<thead>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>no_transaction</th>';
            html += '<th>date</th>';
            html += '<th>action by</th>';
            html += '<th>action at</th>';
            html += '<th>status</th>';
            html += '<th>status at</th>';
            html += '<th>Action</th>';
            html += '</tr>';
            html += '</thead>';

            html += '<tbody>';
            var i;
            var no = 1;
            for(i = 0; i < data.length; i++) {

                var url_detail = base_url + '<?= $this->config->item('index_page'); ?>/order/detail/';

                html += '<tr>';
                html += '<td>' + no + '</td>';

                html += '<td>' + data[i].no_transaction + '</td>';
                html += '<td>' + data[i].date + '</td>';

                // var x;
                // for(x = 0; x < product.length; x++) {
                //     if(product[x].id == data[i].id_product) {
                //         html += '<td>' + product[x].name + '</td>';
                //     }
                // }
                
                html += '<td>' + data[i].action_by + '</td>';
                html += '<td>' + data[i].action_at + '</td>';
                html += '<td>' + data[i].status + '</td>';

                if(data[i].is_processing == 1) {
                    html += '<td>' + data[i].processing_at + '</td>';
                } else if(data[i].is_completed == 1) {
                    html += '<td>' + data[i].completed_at + '</td>';
                } else if(data[i].is_cancel == 1) {
                    html += '<td>' + data[i].cancel_at + '</td>';
                } else {
                    html += '<td>belum di process</td>';
                }

                html += '<td>';
                    html += '<a href="'+ url_detail +''+data[i].no_transaction+'" name="detail_order" class="btn btn-primary detail_order" data-notransaction="'+ data[i].no_transaction +'">Detail</a>';
                html +='</td>';

                html += '</tr>';

                no++;
            }
            html += '</tbody>';

            html += '<tfoot>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>no_transaction</th>';
            html += '<th>date</th>';
            html += '<th>action by</th>';
            html += '<th>action at</th>';
            html += '<th>status</th>';
            html += '<th>status at</th>';
            html += '<th>Action</th>';
            html += '</tr>';
            html += '</tfoot>'

            $('#order_table').html(html);
    }
	
});
</script>
<!-- page script -->
<script>
    $(function () {
        $('#order_table').DataTable()
    })
</script>