<div class="col-md-6">
    <!-- Profile Image -->
    <div class="box box-primary">
        <div class="box-body box-profile">
            <div id="order_detail"></div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->


<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
    var no_transaction = '<?= $no_transaction; ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/order/ajax_get_order_by_no_transaction',
		data: {csrf:csrf, csrf_token:csrf, no_transaction:no_transaction},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

                var html = '<h3 class="profile-username text-center">No Transaction : ' + response.data.no_transaction + '</h3>';
                    html += '<p class="text-muted text-center">Member Order : '+ response.detail.member +'</p>';
                    html += '<p class="text-muted text-center">Product Order : '+ response.detail.product +'</p>';
                    html += '<p class="text-muted text-center">Harga Product Order : '+ response.data.price +'</p>';
                    html += '<p class="text-muted text-center">Jumlah Order : '+ response.data.qty_order +'</p>';
                    html += '<p class="text-muted text-center">Total Order : '+ response.data.total +'</p>';
                    html += '<p class="text-muted text-center">Disetujui oleh : '+ response.detail.action_by +'</p>';
                    html += '<p class="text-muted text-center">Disetuji Tanggal : '+ response.data.action_at +'</p>';
                    html += '<p class="text-muted text-center">Status : '+ response.data.status +'</p>';
                    html += '<p class="text-muted text-center">Waktu Order : '+ response.data.created_at +'</p>';
                    if(response.data.status == "PENDING"){
                        html += '<button type="button" name="processing_order" class="btn btn-success btn-block processing_order" data-notransaction="'+ response.data.no_transaction +'">Processing</button>';
                    } else if(response.data.status == "PROCESSING") {
                        html += '<button type="button" name="completed_order" class="btn btn-success btn-block completed_order" data-notransaction="'+ response.data.no_transaction +'">Completed</button>';
                    }

                $('#order_detail').html(html);

                
            } else {
                alert(response.message.body);
                console.log(response.form_error);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$(".processing_order").on("click",function(){

        var no_transaction = $(this).data('notransaction');

        $.ajax({
            url: base_url + "<?= $this->config->item('index_page'); ?>/order/ajax_action_processing_order",
            method:"POST",
            data:{csrf:csrf, csrf_token:csrf, no_transaction:no_transaction},
            async: false,
            dataType: 'json',
            success:function(response)
            {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }		
                if(!response.result){
                    // foreach(response.form_error)
                    console.log(response.form_error);
                } else {
                    console.log(response.form_error);
                }

            },
            error: function(response) {
                alert('Interrupted');
                console.log(response.message.body);
            }
        });

    });

    $(".completed_order").on("click",function(){

        var no_transaction = $(this).data('notransaction');

        $.ajax({
            url: base_url + "<?= $this->config->item('index_page'); ?>/order/ajax_action_completed_order",
            method:"POST",
            data:{csrf:csrf, csrf_token:csrf, no_transaction:no_transaction},
            async: false,
            dataType: 'json',
            success:function(response)
            {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }		
                if(!response.result){
                    // foreach(response.form_error)
                    console.log(response.form_error);
                } else {
                    console.log(response.form_error);
                }

            },
            error: function(response) {
                alert('Interrupted');
                console.log(response.message.body);
            }
        });

    });
});
</script>