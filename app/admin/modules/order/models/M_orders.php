<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_orders extends MY_Model  {

    public function get_all_order()
    {
        return $this->db->get('m__orders')->result_array();
    }

    public function get_order_by_no_transaction($no_transaction)
    {
        return $this->db->get_where('m__orders', ['no_transaction' => $no_transaction])->row_array();
    }

    public function edit_order($order_data, $no_transaction)
    {
        $this->db->where('no_transaction', $no_transaction);
        
        return $this->db->update('m__orders', $order_data);
    }

}