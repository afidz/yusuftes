<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends MY_Model  {

    public function check_id_member($id_member)
    {
        return $this->db->get_where('m__member', ['id_member' => $id_member])->row_array();
    }

}