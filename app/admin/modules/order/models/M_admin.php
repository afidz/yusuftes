<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends MY_Model  {

    public function check_id_account($id_account)
    {
        return $this->db->get_where('a__admin', ['id_account' => $id_account])->row_array();
    }
    
}