<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends MY_Model  {

    public function get_all_product()
    {
        return $this->db->get('a__product')->result();
    }

    public function check_id_product($id_product)
    {
        return $this->db->get_where('a__product', ['id' => $id_product])->row_array();
    }

    public function edit_stock($id_product, $qty_order)
    {
        $product = $this->check_id_product($id_product);
        $old_stock = $product['stock'];

        $new_stock = $old_stock - $qty_order;

        $data = array(
            'stock' => $new_stock
        );

        $this->db->where('id', $id_product);
        return $this->db->update('a__product', $data);

    }

}