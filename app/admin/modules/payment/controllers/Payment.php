<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session_admin') != TRUE) {
			redirect('login');
		}
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('M_admin', 'M_payment', 'M_member', 'M_member_payment_account', 'M_admin_payment_account'));
    }

    public function index() {
		
		$data['sibebar_menu_active'] = 'payment';
        $data['title'] = 'Payment';
        $data['content_header'] = 'Payment';
        $data['content'] = $this->load->view('/payment','', TRUE);
        
        $this->load->view('template', $data);
    }

    public function detail() {

		$data['no_transaction'] = segment(3);
		$data['sibebar_menu_active'] = 'payment';
        $data['title'] = 'Payment';
        $data['content_header'] = 'Payment';
        $data['content'] = $this->load->view('/detail',$data, TRUE);
        
        $this->load->view('template', $data);
    }

    public function ajax_get_payment_by_no_transaction()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$data = $this->M_payment->get_payment_by_no_transaction(post('no_transaction'));

			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Data Tidak Ditemukan'),
				'form_error' => $data,
				'redirect' => $this->config->item('index_page').'/payment'
			);				
			if($data != TRUE) die(json_encode($json_data));

			$detail = [];
			$member = $this->M_member->check_id_member($data['id_member']);
			$account_member_payment = $this->M_member_payment_account->get_by_id_payment_account($data['m_id_paymentaccount']);
			$account_admin_payment = $this->M_admin_payment_account->get_by_id_payment_account($data['a_id_paymentaccount']);
			
			$detail['member'] = $member['username'];
			$detail['account_bank_member_payment'] = $account_member_payment['account_bank'];
			$detail['account_bank_admin_payment'] = $account_admin_payment['account_bank'];
			$detail['account_number_member_payment'] = $account_member_payment['account_number'];
			$detail['account_number_admin_payment'] = $account_admin_payment['account_number'];

			if($data['action_by'] != "") {
				$admin = $this->M_admin->check_id_account($data['action_by']);
				$detail['action_by'] = $admin['name'];
			} else {
				$detail['action_by'] = "Belum Ada";
			}

			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'data' => $data,
				'detail' => $detail
			);
			print json_encode($json_data);

		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => ''
			);
			print json_encode($json_data);
		}
    }

    public function ajax_get_payment()
	{

		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$data = $this->M_payment->get_all_payment();
					
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'data' => $data
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_payment()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$date_time = date('Y-m-d H:i:s');
			$no_transaction = post('no_transaction');
			$id_member = post('id_member');
			$nominal = post('nominal');

			$payment_data = array(
				'status' => 'PAID',
				'action_by' => $this->session->userdata('id_admin'),
				'action_at' => $date_time,
				'updated_at' => $date_time
			);
			
            $edit = $this->M_payment->edit_payment($payment_data, $no_transaction);

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Data Payment gagal diSetujui!'),
                'form_error' => $edit,
                'redirect' => ''
            );				
			if($edit == FALSE) die(json_encode($json_data));
			
			$balance = $this->M_member->update_balance_by_id($id_member, $nominal);

			$json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Data Balance Member gagal diTambah!'),
                'form_error' => $balance,
                'redirect' => ''
            );				
			if($balance == FALSE) die(json_encode($json_data));

			$json_data = array(
				'csrf_token' => 'csrf token sama',
                'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Data Payment Berhasil DiSetujui'),
				'form_error' => $edit,
                'redirect' => $this->config->item('index_page').'/payment'
			);
            print json_encode($json_data);
            
		} else {

			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
            print json_encode($json_data);
            
		}
	}
}