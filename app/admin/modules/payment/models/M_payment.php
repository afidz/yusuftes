<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_payment extends MY_Model  {

    public function get_all_payment()
    {
        return $this->db->get('m__payment')->result_array();
    }

    public function get_payment_by_no_transaction($no_transaction)
    {
        return $this->db->get_where('m__payment', ['no_transaction' => $no_transaction])->row_array();
    }

    public function edit_payment($payment_data, $no_transaction)
    {
        $this->db->where('no_transaction', $no_transaction);
        
        return $this->db->update('m__payment', $payment_data);
    }

}