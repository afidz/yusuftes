<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin_payment_account extends MY_Model  {

    public function get_by_id_payment_account($id_payment_account)
    {
        return $this->db->get_where('a__payment_account', ['id_payment_account' => $id_payment_account])->row_array();
    }
    
}