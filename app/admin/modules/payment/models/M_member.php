<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends MY_Model  {

    public function check_id_member($id_member)
    {
        return $this->db->get_where('m__member', ['id_member' => $id_member])->row_array();
    }

    public function update_balance_by_id($id_member, $nominal)
    {
        $member = $this->check_id_member($id_member);

        $old_balance = $member['balance'];
        $balance = $old_balance + $nominal;

        $data = array(
            'balance' => $balance
        );
        
        $this->db->where('id_member', $id_member);
        
        return $this->db->update('m__member', $data);

    }

}