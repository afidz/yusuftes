<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Payment</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="payment_table" class="table table-bordered table-striped">
                
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + 'admin.php/payment/ajax_get_payment',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {
                buildTable(response.data);
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});

    function buildTable(data) {
    
        var html = '<thead>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>date</th>';
            html += '<th>no_transaction</th>';
            html += '<th>nominal_transfer</th>';
            html += '<th>status</th>';
            html += '<th>action</th>';
            html += '</tr>';
            html += '</thead>';

            html += '<tbody>';
            var i;
            var no = 1;
            for(i = 0; i < data.length; i++) {

                var url_detail = base_url + '<?= $this->config->item('index_page'); ?>/payment/detail/';

                html += '<tr>';
                html += '<td>' + no + '</td>';
                html += '<td>' + data[i].date + '</td>';
                html += '<td>' + data[i].no_transaction + '</td>';
                html += '<td>' + data[i].nominal_transfer + '</td>';
                html += '<td>' + data[i].status + '</td>';
                html += '<td>';
                    html += '<a href="'+ url_detail +''+data[i].no_transaction+'" name="detail_payment" class="btn btn-primary detail_payment" data-notransaction="'+ data[i].no_transaction +'">Detail</a>';
                html +='</td>';
                html += '</tr>';

                no++;
            }
            html += '</tbody>';

            html += '<tfoot>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>date</th>';
            html += '<th>no_transaction</th>';
            html += '<th>nominal_transfer</th>';
            html += '<th>status</th>';
            html += '<th>action</th>';
            html += '</tr>';
            html += '</tfoot>'

            $('#payment_table').html(html);
    }
	
});
</script>
<!-- page script -->
<script>
    $(function () {
        $('#payment_table').DataTable()
    })
</script>