<div class="col-md-6">
    <!-- Profile Image -->
    <div class="box box-primary">
        <div class="box-body box-profile">
            <div id="payment_detail"></div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->


<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
    var no_transaction = '<?= $no_transaction; ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/payment/ajax_get_payment_by_no_transaction',
		data: {csrf:csrf, csrf_token:csrf, no_transaction:no_transaction},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

                var img_src = base_url + 'assets/images/payment/' + response.data.file_proof;

                var html = '<h3 class="profile-username text-center">No Transaction : ' + response.data.no_transaction + '</h3>';
                    html += '<p class="text-muted text-center">Member Transaction : '+ response.detail.member +'</p>';
                    html += '<p class="text-muted text-center">Account Member : '+response.detail.account_number_member_payment+' ('+ response.detail.account_bank_member_payment +')</p>';
                    html += '<p class="text-muted text-center">Account Admin : '+response.detail.account_number_admin_payment+' ('+ response.detail.account_bank_admin_payment +')</p>';
                    html += '<p class="text-muted text-center">Nominal Transfer : '+ response.data.nominal_transfer +'</p>';
                    html += '<p class="text-muted text-center">Disetujui oleh : '+ response.detail.action_by +'</p>';
                    html += '<p class="text-muted text-center">Disetuji Tanggal : '+ response.data.action_at +'</p>';
                    html += '<p class="text-muted text-center">Status : '+ response.data.status +'</p>';
                    html += '<p class="text-muted text-center">Waktu Transaction : '+ response.data.created_at +'</p>';
                    html += '<img class="img-responsive center-block" src="'+ img_src +'" alt="User profile picture">';
                    if(response.data.status == "PENDING"){
                        html += '<button type="button" name="edit_payment" class="btn btn-success btn-block edit_payment" data-nominal="'+response.data.nominal_transfer+'" data-idmember="'+response.data.id_member+'" data-notransaction="'+ response.data.no_transaction +'">Setujui</button>';
                    }

                $('#payment_detail').html(html);

                
            } else {
                alert(response.message.body);
                console.log(response.form_error);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$(".edit_payment").on("click",function(){

        var no_transaction = $(this).data('notransaction');
        var id_member = $(this).data('idmember');
        var nominal = $(this).data('nominal');

        $.ajax({
            url: base_url + "<?= $this->config->item('index_page'); ?>/payment/ajax_action_edit_payment",
            method:"POST",
            data:{csrf:csrf, csrf_token:csrf, no_transaction:no_transaction, id_member:id_member, nominal:nominal},
            async: false,
            dataType: 'json',
            success:function(response)
            {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }		
                if(!response.result){
                    // foreach(response.form_error)
                    console.log(response.form_error);
                } else {
                    console.log(response.form_error);
                }

            },
            error: function(response) {
                alert('Interrupted');
                console.log(response.message.body);
            }
        });

    });
});
</script>