<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session_admin') != TRUE) {
			redirect('login');
		}
		date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
		$data['sibebar_menu_active'] = 'home';
        $data['title'] = 'Home';
        $data['content_header'] = 'Home';
        $data['content'] = $this->load->view('/home','', TRUE);
        
        $this->load->view('template', $data);
    }

}