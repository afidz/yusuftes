<!-- left column -->
<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Profile</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="" id="myform" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="csrf_token" value="<?= $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="username_token" value="<?= $username; ?>">
            <input type="hidden" name="member_old_profile_image" value="">
            <div class="box-body">
                <div class="form-group">
                    <label for="member_username">Username</label>
                    <input type="text" name="member_username"  class="form-control" id="member_username" placeholder="Enter Username" value="">
                </div>
                <div class="form-group">
                    <label for="member_email">Email address</label>
                    <input type="text" name="member_email"  class="form-control" id="member_email" placeholder="Enter email" value="">
                </div>
                <div class="form-group">
                    <label for="member_password">Password</label>
                    <input type="password" name="member_password"  class="form-control" id="member_password" placeholder="Password" value="">

                    <p class="help-block">Jika password kosong maka password tidak berubah</p>
                </div>
                <div class="form-group">
                    <label for="member_profile_image">File input</label>
                    <input type="file" name="member_profile_image" id="member_profile_image">

                    <p class="help-block">Upload Gambar baru untuk merubah gambar sebelumnya</p>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" name="edit_member" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</div>
<!-- /.col-lg-6 -->


<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
    var username = '<?= $username; ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/member/ajax_get_member_by_username',
		data: {csrf:csrf, csrf_token:csrf, username:username},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

				$('#member_username').val(response.data.username);
				$('#member_email').val(response.data.email);
				$("input[name=member_id]").val(response.data.id_member);
				$("input[name=member_old_profile_image]").val(response.data.profile_image);
                
            } else {
                alert(response.message.body);
                console.log(response.form_error);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('#myform').submit(function(event) {
		event.preventDefault();

		var formData = new FormData(this);
		
		$.ajax({
			type: 'POST',
			url: base_url + '<?= $this->config->item('index_page'); ?>/member/ajax_action_update_member',
			dataType: 'json',
			// data: datastring,
			data: formData,
			processData:false,
			contentType:false,
			cache:false,
			success: function(response) {

				alert(response.message.body);
				if(response.redirect != '') {
					window.location.replace(base_url + response.redirect);
				}		
				if(!response.result){
					// foreach(response.form_error)
					console.log(response.form_error);
				} else {
					console.log(response.form_error);
				}		
			},
			error: function(response) {
				alert('Interrupted');
				console.log(response.message.body);
			}
		});
	});
});
</script>