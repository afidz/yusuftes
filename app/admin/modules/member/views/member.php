<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Member</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="member_table" class="table table-bordered table-striped">
                
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/member/ajax_get_member_data',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

                buildTable(response.data);

            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});

    // $(".delete_member").on("click",function(){
    $(document).on('click','.delete_member',function(){
        $(this).remove();

        var member_id = $(this).data('memberid');

        if(confirm("Yakin ingin menghapus ?")) {
            $.ajax({
                url: base_url + "<?= $this->config->item('index_page'); ?>/member/ajax_action_delete_member",
                method:"POST",
                data:{csrf:csrf, csrf_token:csrf, id_member:member_id},
                async: false,
                dataType: 'json',
                success:function(response)
                {
                    alert(response.message.body);
                    if(response.redirect != '') {
                        window.location.replace(base_url + response.redirect);
                    }		
                    if(!response.result){
                        // foreach(response.form_error)
                        console.log(response.form_error);
                    } else {
                        console.log(response.form_error);
                    }

                    buildTable(response.data);

                },
                error: function() {
                    alert('Interrupted');
                }
            });
        }

    });

    function buildTable(data) {
        var html = '<thead>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>Username</th>';
            html += '<th>Email</th>';
            html += '<th>Action</th>';
            html += '</tr>';
            html += '</thead>';

            html += '<tbody>';
            var i;
            var no = 1;
            for(i = 0; i < data.length; i++) {

                var url_edit = base_url + '<?= $this->config->item('index_page'); ?>/member/edit/';

                html += '<tr>';
                html += '<td>' + no + '</td>';
                html += '<td>' + data[i].username + '</td>';
                html += '<td>' + data[i].email + '</td>';
                html += '<td>';
                    html += '<a href="'+ url_edit +''+data[i].username+'" name="edit_member" class="btn btn-primary edit_member" data-memberid="'+ data[i].id_member +'">Edit</a>';
                    html += '<button type="button" name="delete_member" class="btn btn-danger delete_member" data-memberid="'+ data[i].id_member +'">Delete</button>';
                html +='</td>';
                html += '</tr>';

                no++;
            }
            html += '</tbody>';

            html += '<tfoot>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>Username</th>';
            html += '<th>Email</th>';
            html += '<th>Action</th>';
            html += '</tr>';
            html += '</tfoot>'

            $('#member_table').html(html);
    }
	
});
</script>
<!-- page script -->
<script>
    $(function () {
        $('#member_table').DataTable()
    })
</script>