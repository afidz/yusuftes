<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends MY_Model  {

    public function get_all_member()
    {
        return $this->db->get('m__member')->result();
    }

    public function check_id_member($id_member)
    {
        return $this->db->get_where('m__member', ['id_member' => $id_member])->row_array();
    }

    public function check_email($email)
    {
        return $this->db->get_where('m__member', ['email' => $email])->row_array();
    }

    public function check_username($username)
    {
        return $this->db->get_where('m__member', ['username' => $username])->row_array();
    }

    public function add_data($member_data)
    {
        return $this->db->insert('m__member', $member_data);
    }

    public function update_data($member_data, $member_id)
    {
        if(!empty($_FILES['member_profile_image']['name']))
        {
            $this->_deleteImage($member_id);
            $member_data['profile_image'] = $this->_uploadImage();
        } else {
            $data_member = $this->check_id_member($member_id);
            if(!empty($data_member['profile_image'])) {
                $file_extension = $this->_renameImage($member_id);
                // $this->_renameImage($member_id);
                $member_data['profile_image'] = $this->input->post('member_username', true).".".$file_extension;     
            }
        }

        $this->db->where('id_member', $member_id);
        
        return $this->db->update('m__member', $member_data);
        
        // $this->db->update('m__member', $member_data);
        // return $_FILES;
    }

    private function _uploadImage()
    {
        $config['upload_path'] = './assets/images/reseller/profile'; // path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; // type yang dapat diakses
        $config['file_name'] = $this->input->post('member_username', true);
        $config['overwrite']			= true;
        // $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('member_profile_image')) {
            return $this->upload->data("file_name");
        }
        
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $data_member = $this->check_id_member($id);
        // if ($data_member['profile_image'] != "default.jpg") {
        if (!empty($data_member['profile_image'])) {
            $filename = explode(".", $data_member['profile_image'])[0];
            array_map('unlink', glob(FCPATH."assets/images/reseller/profile/$filename.*"));
            // return array_map('unlink', glob(FCPATH."assets/images/reseller/profile/$filename.*"));
        }
    }

    private function _renameImage($id)
    {
        $data_member = $this->check_id_member($id);

        $filename = $data_member['profile_image'];
        $old_name_image = "assets/images/reseller/profile/$filename";

        $filename_new = $this->input->post('member_username', true);
        $file_extension = explode(".", $data_member['profile_image'])[1];
        $new_name_image = "assets/images/reseller/profile/$filename_new.$file_extension";

        rename($old_name_image, $new_name_image);

        return $file_extension;
    }

    public function delete_member_by_id($id_member)
    {
        $this->_deleteImage($id_member);

        $this->db->where('id_member', $id_member);
        return $this->db->delete('m__member');
    }

}