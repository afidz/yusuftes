<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends MY_Model  {

    public function check_id_member($id_member)
    {
        return $this->db->get_where('a__admin', ['id_member' => $id_member])->row_array();
    }

    public function check_email($email)
    {
        return $this->db->get_where('a__admin', ['email' => $email])->row_array();
    }

    public function check_username($username)
    {
        return $this->db->get_where('a__admin', ['username' => $username])->row_array();
    }

}