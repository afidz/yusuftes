<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session_admin') != TRUE) {
			redirect('login');
		}
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('M_admin', 'M_member'));
    }

    public function index() {

		$data['sibebar_menu_active'] = 'member';
        $data['title'] = 'Member';
        $data['content_header'] = 'Member';
        $data['content'] = $this->load->view('/member','', TRUE);
        
		$this->load->view('template', $data);
		
	}
	
	public function edit()
	{
		$data['sibebar_menu_active'] = 'member';
        $data['title'] = 'Edit Member';
		$data['content_header'] = 'Edit Member';
		$data['username'] = segment(3);
		$data['content'] = $this->load->view('/edit',$data, TRUE);
        
        $this->load->view('template', $data);
	}

	public function ajax_get_member_by_username()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$username = post('username');

			$check_username = $this->M_member->check_username($username);
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Data Tidak Ditemukan'),
				'form_error' => $check_username,
				'redirect' => $this->config->item('index_page').'/member'
			);				
			if($check_username != TRUE) die(json_encode($json_data));

			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
                'data' => $check_username
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
    }

    public function ajax_action_update_member()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$username_token = post('username_token');
			$old_data = $this->M_member->check_username($username_token);
			$id_member = $old_data['id_member'];

			$email = post('member_email', TRUE);
			$username = post('member_username', TRUE);
			$password = post('member_password', TRUE);
			$image = post('member_profile_image', TRUE);
			
			$date_time = date('Y-m-d H:i:s');	

			// cek username baru unique sebelum validation
			if($username == $old_data['username']) {
				$username = '';
			}
			// cek email baru unique sebelum validation
			if($email == $old_data['email']) {
				$email = '';
			}


			$this->form_validation->set_rules('member_email', 'Email', 'valid_email');
			$this->form_validation->set_rules('member_username', 'Username', '');
			
			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {
				$check_email = $this->M_member->check_email($email);
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Email anda telah terdaftar!'),
					'form_error' => $check_email,
					'redirect' => ''
				);				
				if($check_email == TRUE) die(json_encode($json_data));

				
				$check_username = $this->M_member->check_username($username);				
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Username ini telah digunakan!'),
					'form_error' => '',
					'redirect' => ''
				);				
				if($check_username == TRUE) die(json_encode($json_data));

				$member_data = array(
					'email' => post('member_email', TRUE),
					'username' => post('member_username', TRUE),
					'update_at' => $date_time,
				);

				if($password == '') {					
					$password = $old_data['password'];
					$member_data['password'] = $password;
				} else {
					$member_data['password'] = md5($password);
				}

				$update = $this->M_member->update_data($member_data, $id_member);
				
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal diperbarui!'),
					'form_error' => $update,
					'redirect' => ''
				);				
				if($update == FALSE) die(json_encode($json_data));
				
				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses diperbarui!'),
					'form_error' => $update,
					'redirect' => $this->config->item('index_page').'/member'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
    }
    
    public function ajax_action_delete_member()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {

            $delete = $this->M_member->delete_member_by_id(post('id_member'));

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Data Member gagal dihapus!'),
                'form_error' => $delete,
                'redirect' => ''
            );				
            if($delete == FALSE) die(json_encode($json_data));

			$json_data = array(
				'csrf_token' => 'csrf token sama',
                'result' => TRUE,
                'message' => array('head' => 'Success', 'body' => 'Data Member Berhasil Dihapus'),
                'data' => $this->M_member->get_all_member(),
                'redirect' => ''
			);
            print json_encode($json_data);
            
		} else {

			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
            print json_encode($json_data);
            
		}
    }

    public function ajax_get_member_data()
	{

		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
                'data' => $this->M_member->get_all_member()
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
			print json_encode($json_data);
		}
    }

}