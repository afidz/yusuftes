<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session_admin') != TRUE) {
			redirect('login');
		}
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model(array('M_product', 'M_product_type'));
    }

    public function index() {
		$data['sibebar_menu_active'] = 'product';
        $data['title'] = 'Product';
        $data['content_header'] = 'Product';
        $data['content'] = $this->load->view('/product','', TRUE);

        $this->load->view('template', $data);
	}

	public function insert() {
		$data['sibebar_menu_active'] = 'insert';
        $data['title'] = 'Insert Product';
        $data['content_header'] = 'Insert Product';
        $data['content'] = $this->load->view('/insert','', TRUE);

        $this->load->view('template', $data);
	}

	public function edit() {

		$data['id_product'] = segment(3);

		$data['sibebar_menu_active'] = 'edit';
        $data['title'] = 'Edit Product';
        $data['content_header'] = 'Edit Product';
        $data['content'] = $this->load->view('/edit',$data, TRUE);

        $this->load->view('template', $data);
	}

	public function ajax_get_product_by_id()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$product_type = $this->M_product_type->get_all_product_type();
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
                // 'data' => $data
				'data' => $this->M_product->check_id_product(post('id_product')),
				'product_type' => $product_type
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
			print json_encode($json_data);
		}
    }

    public function ajax_get_product()
	{

		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$data = $this->M_product->get_all_product();
			$product_type = $this->M_product_type->get_all_product_type();

			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'data' => $data,
				'product_type' => $product_type
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
	}

	public function ajax_get_product_type()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
                // 'data' => $data
                'data' => $this->M_product_type->get_all_product_type()
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_insert_product()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$product_type = post('product_type', TRUE);
			$name_product = post('name_product', TRUE);
			$price_product = post('price_product', TRUE);
			$markup_product = post('markup_product', TRUE);
			$min_order_product = post('min_order_product', TRUE);
			$max_order_product = post('max_order_product', TRUE);
			$stock_product = post('stock_product', TRUE);
			$description_product = post('description_product', TRUE);

			$date_time = date('Y-m-d H:i:s');

			$this->form_validation->set_rules('product_type', 'Type', 'required');
			$this->form_validation->set_rules('name_product', 'Nama', 'required');
			$this->form_validation->set_rules('price_product', 'Harga', 'required|numeric');
			$this->form_validation->set_rules('markup_product', 'Markup', 'required|numeric');
			$this->form_validation->set_rules('min_order_product', 'Min Order', 'required|numeric');
			$this->form_validation->set_rules('max_order_product', 'Max Order', 'required|numeric');
			$this->form_validation->set_rules('stock_product', 'Stock', 'required|numeric');
			$this->form_validation->set_rules('description_product', 'Description', 'required');
			if (empty($_FILES['image_product']['name']))
			{
				$this->form_validation->set_rules('image_product', 'Image', 'required');
			}


			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {

				$markup_rp = ($price_product * $markup_product) / 100;
				$price_sell = $price_product + $markup_rp;

				$product_data = array(
					'id_type' => $product_type,
					'name' => $name_product,
					'price_hpp' => $price_product,
					'markup' => $markup_product,
					'markup_rp' => $markup_rp,
					'price_sell' => $price_sell,
					'min_order' => $min_order_product,
					'max_order' => $max_order_product,
					'stock' => $stock_product,
					'description' => $description_product,
					'is_active' => 1,
					'created_at' => $date_time
				);
				$insert = $this->M_product->insert_data($product_data);

				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal diperbarui!'),
					'form_error' => $insert,
					'redirect' => ''
				);
				if($insert == FALSE) die(json_encode($json_data));

				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses diinsert!'),
					'form_error' => $insert,
					'redirect' => 'admin.php/product'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_product()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$id_product = post('id_product_token', TRUE);
			$product_type = post('product_type', TRUE);
			$name_product = post('name_product', TRUE);
			$price_product = post('price_product', TRUE);
			$markup_product = post('markup_product', TRUE);
			$min_order_product = post('min_order_product', TRUE);
			$max_order_product = post('max_order_product', TRUE);
			$stock_product = post('stock_product', TRUE);
			$description_product = post('description_product', TRUE);

			$date_time = date('Y-m-d H:i:s');

			$this->form_validation->set_rules('product_type', 'Type', 'required');
			$this->form_validation->set_rules('name_product', 'Nama', 'required');
			$this->form_validation->set_rules('price_product', 'Harga', 'required|numeric');
			$this->form_validation->set_rules('markup_product', 'Markup', 'required|numeric');
			$this->form_validation->set_rules('min_order_product', 'Min Order', 'required|numeric');
			$this->form_validation->set_rules('max_order_product', 'Max Order', 'required|numeric');
			$this->form_validation->set_rules('stock_product', 'Stock', 'required|numeric');
			$this->form_validation->set_rules('description_product', 'Description', 'required');

			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {

				$markup_rp = ($price_product * $markup_product) / 100;
				$price_sell = $price_product + $markup_rp;

				$product_data = array(
					'id_type' => $product_type,
					'name' => $name_product,
					'price_hpp' => $price_product,
					'markup' => $markup_product,
					'markup_rp' => $markup_rp,
					'price_sell' => $price_sell,
					'min_order' => $min_order_product,
					'max_order' => $max_order_product,
					'stock' => $stock_product,
					'description' => $description_product,
					'update_at' => $date_time
				);
				$insert = $this->M_product->edit_data($product_data, $id_product);

				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal diperbarui!'),
					'form_error' => $insert,
					'redirect' => ''
				);
				if($insert == FALSE) die(json_encode($json_data));

				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses diedit!'),
					'form_error' => $insert,
					'redirect' => 'admin.php/product'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_delete_product()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$id_product = post('id_product', TRUE);
			$date_time = date('Y-m-d H:i:s');

			$product_data = array(
				'is_active' => 0,
				'update_at' => $date_time
			);
			$delete = $this->M_product->delete_data($product_data, $id_product);

			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Data gagal didelete!'),
				// 'data' => $data,
				// 'product_type' => $product_type,
				// 'redirect' => ''
			);
			if($delete == FALSE) die(json_encode($json_data));

			$product_type = $this->M_product_type->get_all_product_type();
			$data = $this->M_product->get_all_product();

			$json_data = array(
				'result' => TRUE,
				'message' => array('head' => 'Success', 'body' => 'Data sukses didelete!'),
				'data' => $data,
				'product_type' => $product_type,
				'redirect' => ''
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}

}
