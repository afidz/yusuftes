<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends MY_Model  {

    public function get_all_product()
    {
        return $this->db->get('a__product')->result();
    }

    public function check_id_product($id_product)
    {
        return $this->db->get_where('a__product', ['id' => $id_product])->row_array();
    }

    public function insert_data($product_data)
    {
        $filename = $this->get_unused_image_name();

        $product_data['image'] = $this->_uploadImage($filename);

        return $this->db->insert('a__product', $product_data);
    }

    public function edit_data($product_data, $product_id)
    {
        if(!empty($_FILES['image_product']['name']))
        {
            $this->_deleteImage($product_id);

            $filename = $this->get_unused_image_name();

            $product_data['image'] = $this->_uploadImage($filename);
        }

        $this->db->where('id', $product_id);

        return $this->db->update('a__product', $product_data);
    }

    public function delete_data($product_data, $product_id)
    {
        $this->db->where('id', $product_id);
        return $this->db->update('a__product', $product_data);
    }

    private function _uploadImage($filename)
    {
        $config['upload_path'] = './assets/images/product'; // path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; // type yang dapat diakses
        $config['file_name'] = $filename;
        $config['overwrite']			= true;
        // $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image_product')) {
            return $this->upload->data("file_name");
        }
        
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $product_data = $this->check_id_product($id);
        // if ($product_data['image'] != "default.jpg") {
        if (!empty($product_data['image'])) {
            $filename = explode(".", $product_data['image'])[0];
            array_map('unlink', glob(FCPATH."assets/images/product/$filename.*"));
            // return array_map('unlink', glob(FCPATH."assets/images/reseller/profile/$filename.*"));
        }
    }

    public function get_unused_image_name()
    {
        // Create a random user id between 1200 and 4294967295
        $random_unique_int = 2147483648 + mt_rand( -2147482448, 2147483647 );

        // Make sure the random image name isn't already in use
        $this->db->like('image', $random_unique_int);
        $query = $this->db->get('a__product');
        $result = $query->row_array();

        if( $result > 0 )
        {
            $query->free_result();

            // If the random user_id is already in use, try again
            return $this->get_unused_image_name();
        }

        return $random_unique_int;
    }

}