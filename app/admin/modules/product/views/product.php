<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Product</h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">

            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/product/insert" class="btn btn-success btn-flat">Insert Product</a>
        
            <table id="product_table" class="table table-bordered table-striped"></table>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/product/ajax_get_product',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {
                buildTable(response.data, response.product_type);
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});

    // $(".delete_product").on("click",function(event){
    //     $(this).remove();
    $(document).on('click','.delete_product',function(){
        $(this).remove();

        var product_id = $(this).data('productid');
        
        $.ajax({
            url: base_url + "<?= $this->config->item('index_page'); ?>/product/ajax_action_delete_product",
            method:"POST",
            data:{csrf:csrf, csrf_token:csrf, id_product:product_id},
            async: false,
		    dataType: 'json',
            success:function(response)
            {
                alert(response.message.body);
			
                if(response.result) {
                    buildTable(response.data, response.product_type);
                }

			},
			error: function() {
				alert('Interrupted');
			}
        });

    });

    function buildTable(data, product_type) {
    
        var html = '<thead>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>type</th>';
            html += '<th>name</th>';
            // html += '<th>image</th>';
            html += '<th>price_hpp</th>';
            html += '<th>markup</th>';
            html += '<th>markup_rp</th>';
            html += '<th>price_sell</th>';
            html += '<th>min_order</th>';
            html += '<th>max_order</th>';
            html += '<th>stock</th>';
            html += '<th>Action</th>';
            html += '</tr>';
            html += '</thead>';

            html += '<tbody>';
            var i;
            var no = 1;
            for(i = 0; i < data.length; i++) {

                if(data[i].is_active == 1){

                var url_detail = base_url + '<?= $this->config->item('index_page'); ?>/product/edit/';

                html += '<tr>';
                html += '<td>' + no + '</td>';

                var x;
                for(x = 0; x < product_type.length; x++) {
                    if(product_type[x].id == data[i].id_type) {
                        html += '<td>' + product_type[x].name + '</td>';
                    }
                }

                html += '<td>' + data[i].name + '</td>';
                // html += '<td>' + data[i].image + '</td>';
                html += '<td>Rp.' + data[i].price_hpp + '</td>';
                html += '<td>' + data[i].markup + '%</td>';
                html += '<td>Rp.' + data[i].markup_rp + '</td>';
                html += '<td>Rp.' + data[i].price_sell + '</td>';
                html += '<td>' + data[i].min_order + '</td>';
                html += '<td>' + data[i].max_order + '</td>';
                html += '<td>' + data[i].stock + '</td>';
                html += '<td>';
                    // html += '<a href="'+ url_detail +''+data[i].no_transaction+'" name="detail_payment" class="btn btn-primary detail_payment" data-notransaction="'+ data[i].no_transaction +'">Detail</a>';
                    html += '<a href="'+ url_detail +''+data[i].id+'" name="edit_product" class="btn btn-primary edit_product" data-idproduct="'+ data[i].id +'">Edit</a>';
                    html += '<button type="button" name="delete_product" class="btn btn-danger delete_product" data-productid="'+ data[i].id +'">Delete</button>';
                html +='</td>';
                html += '</tr>';

                no++;

                }
            }
            html += '</tbody>';

            html += '<tfoot>';
            html += '<tr>';
            html += '<th>No</th>';
            html += '<th>type</th>';
            html += '<th>name</th>';
            // html += '<th>image</th>';
            html += '<th>price_hpp</th>';
            html += '<th>markup</th>';
            html += '<th>markup_rp</th>';
            html += '<th>price_sell</th>';
            html += '<th>min_order</th>';
            html += '<th>max_order</th>';
            html += '<th>stock</th>';
            html += '<th>Action</th>';
            html += '</tr>';
            html += '</tfoot>'

            $('#product_table').html(html);
    }
	
});
</script>
<!-- page script -->
<script>
    $(function () {
        $('#product_table').DataTable()
    })
</script>