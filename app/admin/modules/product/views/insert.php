<!-- left column -->
<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Product Insert Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div id="form_error"></div>

        <form action="" id="myform" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="csrf_token" value="<?= $this->security->get_csrf_hash(); ?>">
            <div class="box-body">

                <div class="form-group">
                    <label for="product_type">Select Type Product</label>
                    <select id="product_type" name="product_type" class="form-control">
                    </select>
                </div>

                <div class="form-group">
                    <label for="name_product">Nama Product</label>
                    <input type="text" name="name_product"  class="form-control" id="name_product" placeholder="Enter Nama" value="">
                </div>

                <div class="form-group">
                    <label for="price_product">Harga Product</label>
                    <input type="text" name="price_product"  class="form-control" id="price_product" placeholder="Enter Harga" value="">
                </div>

                <div class="form-group">
                    <label for="markup_product">Markup Product</label>
                    <input type="text" name="markup_product"  class="form-control" id="markup_product" placeholder="Enter Markup" value="">
                </div>

                <div class="form-group">
                    <label for="min_order_product">Min Order Product</label>
                    <input type="text" name="min_order_product"  class="form-control" id="min_order_product" placeholder="Enter Min Order" value="">
                </div>

                <div class="form-group">
                    <label for="max_order_product">Max Order Product</label>
                    <input type="text" name="max_order_product"  class="form-control" id="max_order_product" placeholder="Enter Max Order" value="">
                </div>

                <div class="form-group">
                    <label for="stock_product">Stock Product</label>
                    <input type="text" name="stock_product"  class="form-control" id="stock_product" placeholder="Enter Stock" value="">
                </div>

                <div class="form-group">
                    <label for="description_product">Textarea</label>
                    <textarea name="description_product" id="description_product" class="form-control" rows="3" placeholder="Enter Description Product...."></textarea>
                </div>
                
                <div class="form-group">
                    <label for="image_product">Gambar Product</label>
                    <input type="file" name="image_product" id="image_product">

                    <p class="help-block">Upload Gambar Product</p>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" name="insert_product" class="btn btn-primary">Insert Product</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</div>
<!-- /.col-lg-6 -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/product/ajax_get_product_type',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

				var html = '<option value="">- Select Type -</option>';
                var i;
                for(i = 0; i < response.data.length; i++) {
                    html += '<option value="' + response.data[i].id + '">' + response.data[i].name + '</option>';
                }
                $('#product_type').html(html);

                
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('#myform').submit(function(event) {
		event.preventDefault();

		var formData = new FormData(this);
		
		$.ajax({
			type: 'POST',
			url: base_url + '<?= $this->config->item('index_page'); ?>/product/ajax_action_insert_product',
			dataType: 'json',
			// data: datastring,
			data: formData,
			processData:false,
			contentType:false,
			cache:false,
			success: function(response) {
				alert(response.message.body);
				if(response.redirect != '') {
					window.location.replace(base_url + response.redirect);
				}		
				if(!response.result) {

                    var obj = response.form_error;
                    var result_array = Object.keys(obj).map(function(key) {
                        return [String(key), obj[key]];
                    });

                    var html = '';
                    for(key in obj) {
                        if(obj.hasOwnProperty(key)) {
                            var value = obj[key];
                            html += '<p>' + value + '</p>'
                        }
                    }
                    $('#form_error').html(html);
                    
				} else {
					console.log(response.form_error);
				}		
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});
});
</script>