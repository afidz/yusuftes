<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller  {

	public function index()
	{
        $this->session->unset_userdata('id_admin');
        $this->session->unset_userdata('username');
		$this->session->unset_userdata('do_log_session_admin');
		$this->session->sess_destroy();
		redirect('login', 'refresh');

    }

}
