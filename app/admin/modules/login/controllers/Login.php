<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller  {

	public function __construct() {
        parent::__construct();
        if($this->session->userdata('do_log_session_admin')) {
			redirect('home');
		}
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_admin');
	}

	public function index()
	{
        $data['navbar_nav_active']='login';
		$data['title']='Login';
		$data['content']=$this->load->view('/login',$data,TRUE);

		$this->load->view('template-login', $data);
	}
	
	public function ajax_get_admin_by_login() {
		$user = post('user_admin', TRUE);			
		$password = post('password_admin', TRUE);

		$this->form_validation->set_rules('user_admin', 'Username', 'required');
		$this->form_validation->set_rules('password_admin', 'Password', 'required');

		if($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			// print($error);
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
				'form_error' => $error,
				'redirect' => ''
			);
			print json_encode($json_data);
		} else {
			
			$check_login = $this->M_admin->check_login($user, $password);
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Failed', 'body' => 'Gagal Login Username/Email atau Password Salah'),
				'form_error' => '',
				'redirect' => ''
			);				
			if($check_login == FALSE) die(json_encode($json_data));

			$sesdata = array(
                'id_admin'  => $check_login['id_account'],
                'username' => $check_login['name'],
				'do_log_session_admin' => TRUE
			);
			$this->session->set_userdata($sesdata);

			$json_data = array(
				'result' => TRUE,
				'message' => array('head' => 'From DB', 'body' => 'Berhasil Login'),
				'redirect' => $this->config->item('index_page').'/home'
			);
			print json_encode($json_data);
		}
    }

}
