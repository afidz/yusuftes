<div class="login-box">
    <div class="login-logo">
        <a href="<?= base_url(); ?>"><b>Dodoloid</b> Admin</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="" method="post" id="myform" accept-charset="utf-8">
            <input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash();?>">

            <div class="form-group has-feedback">
                <input type="text" name="user_admin" class="form-control" placeholder="Email/Username">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" name="password_admin" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?= $this->load->view('template-login-footer','', TRUE); ?>

<script type="text/javascript">
    $(document).ready(function() {
        var base_url = '<?php echo base_url(); ?>';
        var csrf = $('#csrf').val();
        
        $('#myform').submit(function(event) {
            event.preventDefault();
            var datastring = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: base_url + '<?= $this->config->item('index_page');?>/login/ajax_get_admin_by_login',
                dataType: 'json',
                data: datastring,
                success: function(response) {
                    alert(response.message.body);
                    
                    if(response.redirect != '') {
                        window.location.replace(base_url + response.redirect);
                    }		
                    if(!response.result){
                        // foreach(response.form_error)
                            console.log(response.form_error);
                    }			
                },
                error: function(response) {
                    alert('Interrupted');
                }
            });
        });
    });
</script>