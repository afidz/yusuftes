<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') != TRUE) {
			redirect('login');
		}
		$this->load->model(array('M_member', 'M_payment', 'M_member_payment_account', 'M_admin_payment_account'));
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		$data['sibebar_menu_active'] = 'payment';
        $data['title'] = 'Payment';
        $data['content_header'] = 'Payment';
        $data['content'] = $this->load->view('/payment','', TRUE);
        
        $this->load->view('template', $data);
	}

	public function balance()
	{
		$data['sibebar_menu_active'] = 'payment';
        $data['title'] = 'Balance';
        $data['content_header'] = 'Balance';
        $data['content'] = $this->load->view('/balance','', TRUE);
        
        $this->load->view('template', $data);
	}
	
	public function ajax_get_payment_account()
	{
		$id_member = $this->session->userdata('id_member');
		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'account' => $this->M_member_payment_account->get_id_payment_account($id_member),
				'admin' => $this->M_admin_payment_account->get_admin_payment_account()
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_insert_payment()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

			$id_member = $this->session->userdata('id_member');

			$account_member = post('payment_account_member', TRUE);
			$account_admin = post('payment_account_dodoloid', TRUE);
			$nominal_transfer = post('payment_nominal', TRUE);
			
			$date_time = date('Y-m-d H:i:s');

			$this->form_validation->set_rules('payment_account_member', 'Account Member', 'required');
			$this->form_validation->set_rules('payment_account_dodoloid', 'Account Admin', 'required');
			$this->form_validation->set_rules('payment_nominal', 'Nominal', 'required|numeric');
			if (empty($_FILES['payment_image_proof']['name']))
			{
				$this->form_validation->set_rules('payment_image_proof', 'Image', 'required');
			}
			
			
			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {
				
				$payment_data = array(
					'date' => $date_time,
					'id_member' => $id_member,
					'm_id_paymentaccount' => $account_member,
					'a_id_paymentaccount' => $account_admin,
					'nominal_transfer' => $nominal_transfer,
					'created_at' => $date_time
				);				
				$insert = $this->M_payment->insert_data($payment_data);
				
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal diinsert!'),
					'form_error' => $insert,
					'redirect' => ''
				);				
				if($insert == FALSE) die(json_encode($json_data));
				
				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses diinsert!'),
					'form_error' => $insert,
					'redirect' => $this->config->item('index_page').'/payment'.'/balance'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}

	public function ajax_get_balance_account()
	{
		$id_member = $this->session->userdata('id_member');
		
		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'data' => $this->M_member->get_balance_account($id_member)
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
	}
}