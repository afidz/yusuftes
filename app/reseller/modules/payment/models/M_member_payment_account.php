<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member_payment_account extends MY_Model  {

    public function get_id_payment_account($id_member)
    {
        return $this->db->get_where('m__payment_account', ['id_member' => $id_member])->result_array();
    }
    
}