<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin_payment_account extends MY_Model  {

    public function get_admin_payment_account()
    {
        return $this->db->get('a__payment_account')->result_array();
    }

}