<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_payment extends MY_Model  {

    public function insert_data($payment_data)
    {

        $payment_data['no_transaction'] = $this->get_unused_payment();

        $payment_data['file_proof'] = $this->_uploadImage($payment_data['no_transaction']);
        return $this->db->insert('m__payment', $payment_data);
    }

    private function _uploadImage($filename)
    {
        $config['upload_path'] = './assets/images/payment'; // path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; // type yang dapat diakses
        $config['file_name'] = $filename;
        $config['overwrite']			= true;
        // $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('payment_image_proof')) {
            return $this->upload->data("file_name");
        }
        
        return "default.jpg";
    }

    public function get_unused_payment()
    {
        // Create a random user id between 1200 and 4294967295
        $random_unique_int = 2147483648 + mt_rand( -2147482448, 2147483647 );

        // Make sure the random user_id isn't already in use
        $query = $this->db->get_where( 'm__payment', ['no_transaction' => $random_unique_int] );

        $result = $query->row_array();

        if( $result > 0 )
        {
            $query->free_result();

            // If the random user_id is already in use, try again
            return $this->get_unused_payment();
        }

        return $random_unique_int;
    }
}