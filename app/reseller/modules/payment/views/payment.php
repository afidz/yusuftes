<!-- left column -->
<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Payment Form</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div id="form_error"></div>

        <form action="" id="myform" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="csrf_token" value="<?= $this->security->get_csrf_hash(); ?>">
            <div class="box-body">

                <div class="form-group">
                    <label for="payment_account_member">Account Reseller</label>
                    <select id="payment_account_member" name="payment_account_member" class="form-control">
                    </select>
                </div>

                <div class="form-group">
                    <label for="payment_account_dodoloid">Account Admin</label>
                    <select id="payment_account_dodoloid" name="payment_account_dodoloid" class="form-control">
                    </select>
                </div>

                <div class="form-group">
                    <label for="payment_nominal">Nominal</label>
                    <input type="text" name="payment_nominal"  class="form-control" id="payment_nominal" placeholder="Enter Nominal" value="">
                </div>
                
                <div class="form-group">
                    <label for="payment_image_proof">File input</label>
                    <input type="file" name="payment_image_proof" id="payment_image_proof">

                    <p class="help-block">Upload Gambar bukti tranfser</p>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" name="payment_totup" class="btn btn-primary">Payment Topup</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</div>
<!-- /.col-lg-6 -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/payment/ajax_get_payment_account',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

				var html = '<option value="">- Select Account Reseller -</option>';
                var i;
                for(i = 0; i < response.account.length; i++) {
                    html += '<option value="' + response.account[i].id_payment_account + '">' + response.account[i].account_bank + '</option>';
                }
                $('#payment_account_member').html(html);

                var html = '<option value="">- Select Account Admin -</option>';
                var x;
                for(x = 0; x < response.admin.length; x++) {
                    html += '<option value="' + response.admin[x].id_payment_account + '">' + response.admin[x].account_bank + '</option>';
                }
                $('#payment_account_dodoloid').html(html);

                
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('#myform').submit(function(event) {
		event.preventDefault();

		var formData = new FormData(this);
		
		$.ajax({
			type: 'POST',
			url: base_url + '<?= $this->config->item('index_page'); ?>/payment/ajax_action_insert_payment',
			dataType: 'json',
			// data: datastring,
			data: formData,
			processData:false,
			contentType:false,
			cache:false,
			success: function(response) {
				alert(response.message.body);
				if(response.redirect != '') {
					window.location.replace(base_url + response.redirect);
				}		
				if(!response.result) {

                    var obj = response.form_error;
                    var result_array = Object.keys(obj).map(function(key) {
                        return [String(key), obj[key]];
                    });

                    var html = '';
                    for(key in obj) {
                        if(obj.hasOwnProperty(key)) {
                            var value = obj[key];
                            html += '<p>' + value + '</p>'
                        }
                    }
                    $('#form_error').html(html);
                    
				} else {
					console.log(response.form_error);
				}		
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});
});
</script>