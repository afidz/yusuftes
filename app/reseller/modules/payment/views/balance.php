<div class="col-md-6">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Saldo</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <dl>
            <dt>Total Saldo</dt>
            <dd><h3 id="account_balance"></h3></dd>
            </dl>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <a href="<?= base_url(); ?><?= $this->config->item('index_page'); ?>/payment" class="btn btn-primary btn-flat">Tambah Saldo</a>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</div>
<!-- ./col -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/payment/ajax_get_balance_account',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

                if(response.data.balance == null) {
                    var html = 'Rp. 0';
                } else {
                    var html = 'Rp.'+ response.data.balance;
                }
                $('#account_balance').html(html);
                
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
});
</script>