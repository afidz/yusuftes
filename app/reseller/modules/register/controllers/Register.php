<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') == TRUE) {
			redirect('home');
		}
		$this->load->model('M_member');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		$data['navbar_nav_active']='register';
		$data['title']='Register';
		$data['content']=$this->load->view('/register',$data,TRUE);

		$this->load->view('template-login', $data);
	}

    public function ajax_action_member_register() {
		
        $email = post('member_email', TRUE);
        $username = post('member_username', TRUE);
        $password = post('member_password', TRUE);
        
        $date_time = date('Y-m-d H:i:s');
        
        $this->form_validation->set_rules('member_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('member_username', 'Username', 'required');
        $this->form_validation->set_rules('member_password', 'Password', 'required');
        
        if($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
                'form_error' => $error,
                'redirect' => ''
            );
            print json_encode($json_data);
        } else {
            $check_email = $this->M_member->check_email($email);
            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Email anda telah terdaftar!'),
                'form_error' => '',
                'redirect' => ''
            );				
            if($check_email == TRUE) die(json_encode($json_data));
            
            $check_username = $this->M_member->check_username($username);				
            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Username ini telah digunakan!'),
                'form_error' => '',
                'redirect' => ''
            );				
            if($check_username == TRUE) die(json_encode($json_data));
            
            $member_data = array(
                'email' => $email,
                'created_at' => $date_time,
                'username ' => $username,
                'password' => md5($password),
                'created_at' => $date_time,
            );				
            $insert = $this->M_member->add_data($member_data);
            
            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Data gagal disimpan!'),
                'form_error' => '',
                'redirect' => ''
            );				
            if($insert == FALSE) die(json_encode($json_data));
            
            $json_data = array(
                'result' => TRUE,
                'message' => array('head' => 'Success', 'body' => 'Data sukses disimpan!'),
                'form_error' => '',
                'redirect' => $this->config->item('index_page').'/login'
            );
            print json_encode($json_data);
        }
	}

}