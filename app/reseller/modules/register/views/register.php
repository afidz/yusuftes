<div class="register-box">
    <div class="register-logo">
        <a href="<?= base_url(); ?>"><b>Dodolo</b>id</a>
    </div>
    <!-- /.register-logo -->
    <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>

        <form action="" method="post" id="myform" accept-charset="utf-8">
            <input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash();?>">

            <div class="form-group has-feedback">
                <input type="text" name="member_username" class="form-control" placeholder="Username">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="text" name="member_email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" name="member_password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="login.html" class="text-center">I already have a membership</a>

    </div>
    <!-- /.register-box-body -->
</div>
<!-- /.register-box -->

<?= $this->load->view('template-login-footer','', TRUE); ?>

<script type="text/javascript">
    $(document).ready(function() {
        var base_url = '<?php echo base_url(); ?>';
        
        $('#myform').submit(function(event) {
            event.preventDefault();
            var datastring = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: base_url + '<?= $this->config->item('index_page');?>/register/ajax_action_member_register',
                dataType: 'json',
                data: datastring,
                success: function(response) {
                    alert(response.message.body);
                    if(response.redirect != '') {
                        window.location.replace(base_url + response.redirect);
                    }		
                    if(!response.result){
                        // foreach(response.form_error)
                        console.log(response.form_error); // memberikan feedback input apa yg harus di isi
                    }			
                },
                error: function(response) {
                    alert('Interrupted');
                }
            });
        });
    });
</script>