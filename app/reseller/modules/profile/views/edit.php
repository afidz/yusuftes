<!-- left column -->
<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Profile</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="" id="myform" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="csrf_token" value="<?= $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="member_old_profile_image" value="">
            <div class="box-body">
                <div class="form-group">
                    <label for="member_username">Username</label>
                    <input type="text" name="member_username"  class="form-control" id="member_username" placeholder="Enter Username" value="">
                </div>
                <div class="form-group">
                    <label for="member_email">Email address</label>
                    <input type="text" name="member_email"  class="form-control" id="member_email" placeholder="Enter email" value="">
                </div>
                <div class="form-group">
                    <label for="member_password">Password</label>
                    <input type="password" name="member_password"  class="form-control" id="member_password" placeholder="Password" value="">

                    <p class="help-block">Jika password kosong maka password tidak berubah</p>
                </div>
                <div class="form-group">
                    <label for="member_profile_image">File input</label>
                    <input type="file" name="member_profile_image" id="member_profile_image">

                    <p class="help-block">Upload Gambar baru untuk merubah gambar sebelumnya</p>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" name="edit_member" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</div>
<!-- /.col-lg-6 -->


<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/profile/ajax_get_reseller_profile',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

				$('#member_username').val(response.profile.username);
				$('#member_email').val(response.profile.email);
				$("input[name=member_id]").val(response.profile.id_member);
				$("input[name=member_old_profile_image]").val(response.profile.profile_image);

                
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
	$('#myform').submit(function(event) {
		event.preventDefault();

		var formData = new FormData(this);
		
		$.ajax({
			type: 'POST',
			url: base_url + '<?= $this->config->item('index_page'); ?>/profile/ajax_action_update_profile',
			dataType: 'json',
			// data: datastring,
			data: formData,
			processData:false,
			contentType:false,
			cache:false,
			success: function(response) {

				alert(response.message.body);
				if(response.redirect != '') {
					window.location.replace(base_url + response.redirect);
				}		
				if(!response.result){
					// foreach(response.form_error)
					console.log(response.form_error); // untuk mengcheck kesalahan input data
				} else {
					console.log(response.form_error); // untuk mengcheck kesalahan input data
				}		
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});
});
</script>