<div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
        <div class="box-body box-profile">
            <div id="reseller_profile"></div>        

            <a href="<?= base_url(); ?><?= $this->config->item('index_page');?>/profile/edit" class="btn btn-primary btn-block"><b>Edit</b></a>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/profile/ajax_get_reseller_profile',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {

                if(response.profile.profile_image == "") {
                    var img_src = base_url + 'assets/images/reseller/profile/default-160x160.png';    
                } else {
                    var img_src = base_url + 'assets/images/reseller/profile/' + response.profile.profile_image;
                }

                var html = '<img class="profile-user-img img-responsive img-circle" src="'+ img_src +'" alt="User profile picture">';
                    html += '<h3 class="profile-username text-center">' + response.profile.username + '</h3>';
                    html += '<p class="text-muted text-center">'+ response.profile.email +'</p>';

                $('#reseller_profile').html(html);
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});
	
});
</script>