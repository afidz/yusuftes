<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') != TRUE) {
			redirect('login');
		}
        $this->load->model('M_member');
        date_default_timezone_set('Asia/Jakarta');
    }
    
	public function index()
	{
		$data['sibebar_menu_active'] = 'profile';
        $data['title'] = 'Profile';
        $data['content_header'] = 'Profile';
        $data['content'] = $this->load->view('/profile','', TRUE);
        
        $this->load->view('template', $data);
    }

    public function edit()
	{
        $data['sibebar_menu_active'] = 'profile';
        $data['title'] = 'Edit Profile';
        $data['content_header'] = 'Edit Profile';
        $data['content'] = $this->load->view('/edit','', TRUE);
        
        $this->load->view('template', $data);
	}

    public function ajax_get_reseller_profile()
	{
		$id_member = $this->session->userdata('id_member');

		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'profile' => $this->M_member->check_id_member($id_member)
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/auth'
			);
			print json_encode($json_data);
		}
    }
    
    public function ajax_action_update_profile()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$id_member = $this->session->userdata('id_member');
			$email = post('member_email', TRUE);
			$username = post('member_username', TRUE);
			$password = post('member_password', TRUE);
			$image = post('member_profile_image', TRUE);

			$date_time = date('Y-m-d H:i:s');

			$old_data = $this->M_member->check_id_member($id_member);
			// cek username baru unique sebelum validation
			if($username == $old_data['username']) {
				$username = '';
			}
			// cek email baru unique sebelum validation
			if($email == $old_data['email']) {
				$email = '';
			}


			$this->form_validation->set_rules('member_email', 'Email', 'valid_email');
			$this->form_validation->set_rules('member_username', 'Username', '');

			if($this->form_validation->run() == FALSE) {
				$error = $this->form_validation->error_array();
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Mohon lengkapi form yang tersedia!'),
					'form_error' => $error,
					'redirect' => ''
				);
				print json_encode($json_data);
			} else {
				$check_email = $this->M_member->check_email($email);
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Email anda telah terdaftar!'),
					'form_error' => $check_email,
					'redirect' => ''
				);
				if($check_email == TRUE) die(json_encode($json_data));


				$check_username = $this->M_member->check_username($username);
				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Username ini telah digunakan!'),
					'form_error' => '',
					'redirect' => ''
				);
				if($check_username == TRUE) die(json_encode($json_data));

				$member_data = array(
					'email' => post('member_email', TRUE),
					'username' => post('member_username', TRUE),
					'update_at' => $date_time,
				);

				if($password == '') {
					$password = $old_data['password'];
					$member_data['password'] = $password;
				} else {
					$member_data['password'] = md5($password);
				}

				$update = $this->M_member->update_data($member_data, $id_member);

				$json_data = array(
					'result' => FALSE,
					'message' => array('head' => 'Failed', 'body' => 'Data gagal diperbarui!'),
					'form_error' => $update,
					'redirect' => ''
				);
				if($update == FALSE) die(json_encode($json_data));

				$json_data = array(
					'result' => TRUE,
					'message' => array('head' => 'Success', 'body' => 'Data sukses diperbarui!'),
					'form_error' => $update,
					'redirect' => $this->config->item('index_page').'/profile'
				);
				print json_encode($json_data);
			}
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}
    
}