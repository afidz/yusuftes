<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') != TRUE) {
			redirect('login');
        }
        $this->load->model(array('M_member', 'M_orders', 'M_product', 'M_product_type'));
		date_default_timezone_set('Asia/Jakarta');
    }
    
	public function index()
	{
		$data['sibebar_menu_active'] = 'order';
        $data['title'] = 'Order';
        $data['content_header'] = 'Order';
        $data['content'] = $this->load->view('/order','', TRUE);
        
        $this->load->view('template', $data);
    }

    public function riwayat()
	{
		$data['sibebar_menu_active'] = 'order';
        $data['title'] = 'Riwayat Order';
        $data['content_header'] = 'Riwayat Order';
        $data['content'] = $this->load->view('/riwayat','', TRUE);
        
        $this->load->view('template', $data);
    }

    public function ajax_get_order_by_id_member()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {
			// $data = $this->M_product->get_all_product();
            // $product_type = $this->M_product_type->get_all_product_type();
            
            $id_member = $this->session->userdata('id_member');
            $data = $this->M_orders->get_order_by_id_member($id_member);

            $product = $this->M_product->get_all_product();
					
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
                'data' => $data,
                'product' => $product
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
    }
    
    public function ajax_get_product()
    {
        if(post('csrf_token') == $this->security->get_csrf_hash()) {
			$data = $this->M_product->get_all_product();
			$product_type = $this->M_product_type->get_all_product_type();
					
			$json_data = array(
				'csrf_token' => 'csrf token sama',
				'result' => TRUE,
				'data' => $data,
				'product_type' => $product_type
			);
			print json_encode($json_data);
		} else {
			$json_data = array(
				'csrf_token' => 'csrf token tidak sama',
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'redirect' => $this->config->item('index_page').'/login'
			);
			print json_encode($json_data);
		}
    }

    public function ajax_action_insert_order()
	{
		if(post('csrf_token') == $this->security->get_csrf_hash()) {

            $id_member = $this->session->userdata('id_member');

            $id_product = post('id_product', TRUE);
            $qty_order = post('jumlah_order', TRUE);
            $price = post('price_product', TRUE);
            $total = $price * $qty_order;

            $date_time = date('Y-m-d H:i:s');

            $min_order = post('min_order_product', TRUE) - 1;
            $max_order = post('max_order_product', TRUE) + 1;

            $balance = $this->M_member->get_balance_account($id_member);

            // var_dump($balance <= $total);

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Saldo Tidak Cukup'),                    
                'redirect' => ''
            );
            if($balance <= $total) die(json_encode($json_data));

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Jumlah Order Tidak Diperbolehkan'),                    
                'redirect' => ''
            );
            if(($qty_order <= $max_order) && ($min_order >= $qty_order)) die(json_encode($json_data));

            $order_data = array(
                'id_product' => $id_product,
                'id_member' => $id_member,
                'date' => $date_time,
                'qty_order' => $qty_order,
                'price' => $price,
                'total' => $total,
                // 'is_processing' => '1',
                'status' => 'PENDING',
                'created_at' => $date_time
            );

            $insert = $this->M_orders->insert_data($order_data);

            $json_data = array(
                'result' => FALSE,
                'message' => array('head' => 'Failed', 'body' => 'Order gagal dilakukan!'),
                'form_error' => $insert,
                'redirect' => ''
            );
            if($insert == FALSE) die(json_encode($json_data));

            $edit_stock = $this->M_product->edit_stock($id_product, $qty_order);
            $edit_balance = $this->M_member->edit_balance($id_member, $total);

            $json_data = array(
                'result' => TRUE,
                'message' => array('head' => 'Success', 'body' => 'Order sukses dilakukan!'),
                'form_error' => $insert,
                'redirect' => $this->config->item('index_page').'/order'
            );
            print json_encode($json_data);
			
		} else {
			$json_data = array(
				'result' => FALSE,
				'message' => array('head' => 'Denied', 'body' => 'Akses ditolak!'),
				'form_error' => '',
				'redirect' => ''
			);
			print json_encode($json_data);
		}
	}

}
