<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_type extends MY_Model  {

    public function get_all_product_type()
    {
        return $this->db->get('a__product_type')->result();
    }

}