<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends MY_Model  {

    public function get_balance_account($id_member)
    {
        $balance = $this->db->get_where('m__member', ['id_member' => $id_member])->row_array();

        return $balance['balance'];
    }

    public function edit_balance($id_member, $total_order)
    {
        $old_balance = $this->get_balance_account($id_member);

        $new_balance = $old_balance - $total_order;

        $data = array(
            'balance' => $new_balance
        );

        $this->db->where('id_member', $id_member);
        return $this->db->update('m__member', $data);

    }
    
}