<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_orders extends MY_Model  {

    public function get_order_by_id_member($id_member)
    {
        return $this->db->get_where( 'm__orders', ['id_member' => $id_member] )->result_array();
    }

    public function insert_data($order_data)
    {
        $order_data['no_transaction'] = $this->get_unused_order();

        return $this->db->insert('m__orders', $order_data);
    }

    public function get_unused_order()
    {
        // Create a random user id between 1200 and 4294967295
        $random_unique_int = 2147483648 + mt_rand( -2147482448, 2147483647 );

        // Make sure the random user_id isn't already in use
        $query = $this->db->get_where( 'm__orders', ['no_transaction' => $random_unique_int] );

        $result = $query->row_array();

        if( $result > 0 )
        {
            $query->free_result();

            // If the random user_id is already in use, try again
            return $this->get_unused_order();
        }

        return $random_unique_int;
    }
    
}