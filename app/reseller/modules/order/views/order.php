<div id="product_list"></div>

<?= $this->load->view('template-footer', '', TRUE); ?>

<script type="text/javascript">
$(document).ready(function() {
	var base_url = '<?php echo base_url(); ?>';
	var csrf = '<?= $this->security->get_csrf_hash(); ?>';
	
	$.ajax({
		type: 'POST',
		url: base_url + '<?= $this->config->item('index_page'); ?>/order/ajax_get_product',
		data: {csrf:csrf, csrf_token:csrf},
		async: false,
		dataType: 'json',
		success: function(response) {
            if(response.result) {
                buildOrder(response.data, response.product_type);
            } else {
                alert(response.message.body);
                if(response.redirect != '') {
                    window.location.replace(base_url + response.redirect);
                }
            }
		},
		error: function() {
			alert('Interrupted');
		}
	});

    // $('#myform').submit(function(event) {
    $(document).on('submit', '#myform', function(event) {

        event.preventDefault();
        var datastring = $(this).serialize();
		
		$.ajax({
			type: 'POST',
			url: base_url + '<?= $this->config->item('index_page'); ?>/order/ajax_action_insert_order',
			dataType: 'json',
            data: datastring,
			success: function(response) {
				alert(response.message.body);
				if(response.redirect != '') {
					window.location.replace(base_url + response.redirect);
				}
				
			},
			error: function() {
				alert('Interrupted');
			}
		});
	});

    function buildOrder(data, product_type) {

        var html = '';

        var i;
        for(i = 0; i < data.length; i++) {

            if(data[i].is_active == 1) {

            html += '<div class="col-md-4">';
                html += '<div class="box box-widget">';

                    html += '<div class="box-header with-border">';
                        html += '<div class="user-block">';
                            html += '<span class="username">'+ data[i].name +'</span>';
                        html += '</div>';
                    html += '</div>';

                    html += '<div class="box-body">';
                        html += '<img class="img-responsive pad" src="<?= base_url(); ?>assets/images/product/'+ data[i].image +'" alt="Photo">';
                        html += '<p>'+ data[i].description +'</p>';
                    html += '</div>';

                    html += '<div class="box-footer box-comments">';
                        html += '<div class="box-comment">';
                            html += '<div>'
                                html += '<span class="username">Jenis :</span>';

                                var x;
                                for(x = 0; x < product_type.length; x++) {
                                    if(product_type[x].id == data[i].id_type) {
                                        html += product_type[x].name;
                                    }
                                }

                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Harga Hpp :</span>';
                                html += 'Rp.'+ data[i].price_hpp;
                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Markup :</span>';
                                html += data[i].markup +'%';
                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Keuntungan :</span>';
                                html += 'Rp.'+ data[i].markup_rp;
                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Harga Jual :</span>';
                                html += 'Rp.'+ data[i].price_sell;
                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Min Order :</span>';
                                html += data[i].min_order;
                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Max Order :</span>';
                                html += data[i].max_order;
                            html += '</div>';

                            html += '<div>'
                                html += '<span class="username">Stock:</span>';
                                html += data[i].stock;
                            html += '</div>';

                        html += '</div>';    
                    html += '</div>';
                    
                    html += '<div class="box-footer">';
                        html += '<form action="" id="myform" method="post">';
                            html += '<input type="hidden" name="csrf" value="<?= $this->security->get_csrf_hash(); ?>">';
                            html += '<input type="hidden" name="csrf_token" value="<?= $this->security->get_csrf_hash(); ?>">';
                            html += '<input type="hidden" name="id_product" value="'+ data[i].id +'">';
                            html += '<input type="hidden" name="price_product" value="'+ data[i].price_hpp +'">';
                            html += '<input type="hidden" name="min_order_product" value="'+ data[i].min_order +'">';
                            html += '<input type="hidden" name="max_order_product" value="'+ data[i].max_order +'">';
                            html += '<div class="input-group">';
                                html += '<input type="number" name="jumlah_order" class="form-control input-sm" placeholder="Jumlah Order" val="" required>';
                                // html += '<input type="submit" name="order_product" value="order" class="btn btn-success btn-sm">';
                                html += '<span class="input-group-btn">';
                                    html += '<button type="submit" name="order_product" class="btn btn-success btn-flat btn-sm">Order</button>';
                                html += '</span>';
                            html += '</div>';
                        html += '</form>';
                    html += '</div>';

                html += '</div>';
            html += '</div>';

            } // end if
            
        } //end for

        $('#product_list').html(html);
    }
	
});
</script>
