<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller  {

	public function __construct() {
		parent::__construct();
		if($this->session->userdata('do_log_session') != TRUE) {
			redirect('login');
		}
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		$this->session->unset_userdata('id_member');
		$this->session->unset_userdata('do_log_session');
		$this->session->sess_destroy();
		redirect('login', 'refresh');
    }
    
}