<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dodoloid | <?= $title ?></title>

  <?= $this->load->view('template-login-head','', TRUE); ?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?= base_url(); ?>" class="navbar-brand"><b>Dodolo</b>id</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <?php if($this->session->userdata('do_log_session')): ?>
            <?php else:?>
              <li class="<?php if($navbar_nav_active == "login"){echo "active";}?>"><a href="<?= base_url();?><?= $this->config->item('index_page'); ?>/login">Login</a></li>
              <li class="<?php if($navbar_nav_active == "register"){echo "active";}?>"><a href="<?= base_url();?><?= $this->config->item('index_page'); ?>/register">Register</a></li>
            <?php endif; ?>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">

        <?= $content; ?>


  </body>
</html>
