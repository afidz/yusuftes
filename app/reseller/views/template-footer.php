                        
                    </div>
                    <!-- /.row -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                reserved.
            </footer>

        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>

        <!-- jQuery UI 1.11.4 -->
        <script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
        $.widget.bridge('uibutton', $.ui.button);
        </script>

        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>

        <!-- DataTables -->
        <script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/dataTables.bootstrap.min.js"></script>

        <!-- page script -->
        <script>
            $(function () {
                $('#example1').DataTable()
                $('#example2').DataTable({
                    'paging'      : true,
                    'lengthChange': false,
                    'searching'   : false,
                    'ordering'    : true,
                    'info'        : true,
                    'autoWidth'   : false
                })
            })
        </script>